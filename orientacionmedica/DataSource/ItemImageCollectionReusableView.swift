//
//  ItemImageCollectionReusableView.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/1/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ItemImageCollectionReusableView: UICollectionReusableView {

    
    @IBOutlet weak var mItemImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
