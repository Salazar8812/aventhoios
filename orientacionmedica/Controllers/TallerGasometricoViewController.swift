//
//  TallerGasometricoViewController.swift
//  orientacionmedica
//
//  Created by Branchbit on 19/03/21.
//  Copyright © 2021 avento. All rights reserved.
//

import UIKit

class TallerGasometricoViewController: BaseViewController {
    @IBOutlet weak var mInput1TexfField: UITextField!
    @IBOutlet weak var mInput2TextField: UITextField!
    @IBOutlet weak var mInput3TextField: UITextField!
    @IBOutlet weak var mInput4TextField: UITextField!
    @IBOutlet weak var mInput5TextField: UITextField!
    @IBOutlet weak var mInput6TextField: UITextField!
    @IBOutlet weak var mInput7TextField: UITextField!
    @IBOutlet weak var mInput8TextField: UITextField!
    
    @IBOutlet weak var mCalcularButton: UIButton!
    
    
    var mResult1 : Double = 0.0
    var mResult2 : Double = 0.0
    var mResult3 : Double = 0.0
    var mResult4 : Double = 0.0
    var mResult5 : Double = 0.0
    var mResult6 : Double = 0.0
    var mResult7 : Double = 0.0
    var mResult8 : Double = 0.0
    var mResult9 : Double = 0.0
    
    var mInput1 : Double = 0.0
    var mInput2 : Double = 0.0
    var mInput3 : Double = 0.0
    var mInput4 : Double = 0.0
    var mInput5 : Double = 0.0
    var mInput6 : Double = 0.0
    var mInput7 : Double = 0.0
    var mInput8 : Double = 0.0
    
    var mListField : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addField()
        addTargets()
        setData()
    }
    
    func addTargets(){
        mCalcularButton.addTarget(self, action:#selector(calcular) , for: .touchUpInside)
    }
    
    func setData(){
        mInput1 = mInput1TexfField.text != "" ? mInput1TexfField.text!.toDouble()! : 0.0
        mInput2 = mInput2TextField.text != "" ? mInput2TextField.text!.toDouble()! : 0.0
        mInput3 = mInput3TextField.text != "" ? mInput3TextField.text!.toDouble()! : 0.0
        mInput4 = mInput4TextField.text != "" ? mInput4TextField.text!.toDouble()! : 0.0
        mInput5 = mInput5TextField.text != "" ? mInput5TextField.text!.toDouble()! : 0.0
        mInput6 = mInput6TextField.text != "" ? mInput6TextField.text!.toDouble()! : 0.0
        mInput7 = mInput7TextField.text != "" ? mInput7TextField.text!.toDouble()! : 0.0
        mInput8 = mInput8TextField.text != "" ? mInput8TextField.text!.toDouble()! : 0.0
    }
    
    func addField(){
        mListField.append(mInput1TexfField)
        mListField.append(mInput2TextField)
        mListField.append(mInput3TextField)
        mListField.append(mInput4TextField)
        mListField.append(mInput5TextField)
        mListField.append(mInput6TextField)
        mListField.append(mInput7TextField)
        mListField.append(mInput8TextField)
    }
    
    func executeForms(){
        form1()
        form2()
        form3()
        form4()
        form5()
        form6()
        form7()
        form8()
        form9()
    }
    
    @objc func calcular(){
        print("Click")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResultFormsTallerViewController") as! ResultFormsTallerViewController
        setData()
        executeForms()
        vc.mListResult.append(mResult1.toString())
        vc.mListResult.append(mResult2.toString())
        vc.mListResult.append(mResult3.toString())
        vc.mListResult.append(mResult4.toString())
        vc.mListResult.append(mResult5.toString())
        vc.mListResult.append(mResult6.toString3())
        vc.mListResult.append(mResult7.toString3())
        vc.mListResult.append(mResult8.toString())
        vc.mListResult.append(mResult9.toString())
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func form1(){
        let firstResult = (mInput8 - mInput2);
        mResult1 = firstResult;
        mResult1 = replaceDat(mRes: mResult1.isNaN || mResult1.isInfinite ? 0.0 : mResult1)
              
    }
    
    func form2(){
        let firstResult = (mInput1 - mInput8);
        mResult2 = firstResult;
        mResult2 = replaceDat(mRes: mResult2.isNaN || mResult2.isInfinite ? 0.0 : mResult2)
    }
    
    func form3(){
        let firstResult = (mInput3 / mResult1)
        mResult3 = firstResult;
        mResult3 = replaceDat(mRes: mResult3.isNaN || mResult3.isInfinite ? 0.0 : mResult3)
    }
    
    func form4(){
        let firstResult = (mInput1 - mInput2);
        mResult4 = (mInput3 / firstResult)
        mResult4 = replaceDat(mRes: mResult4.isNaN || mResult4.isInfinite ? 0.0 : mResult4)
    }
    
    func form5(){
        let firstResult = (mInput4 / 60)
        let secondResult = mResult2 / firstResult;
        mResult5 = secondResult;
        mResult5 = replaceDat(mRes: mResult5.isNaN || mResult5.isInfinite ? 0.0 : mResult5)
        
    }
    
    func form6(){
        print("Resultado 3 ", mResult3)
        mResult6 = 1 / mResult3;
        mResult6 = replaceDat14(mRes: mResult6.isNaN || mResult6.isInfinite ? 0.0 : mResult6)
    }
    
    func form7(){
        mResult7 = 1 / mResult4;
        mResult7 = replaceDat(mRes: mResult7.isNaN || mResult7.isInfinite ? 0.0 : mResult7)
    }
    
    func form8(){
        let firstResult = mResult1 / 2;
        let secondResult = mInput1 - firstResult
        let threeResult =  mInput3 * 0.001
        let fourResult = threeResult * secondResult * mInput5 * 0.098;
        mResult8 = fourResult;
        mResult8 = replaceDat(mRes: mResult8.isNaN || mResult8.isInfinite ? 0.0 : mResult8)
    }
    
    func form9(){
        let firstResult = (mInput5 * mInput3);
        let secondResult = firstResult * mInput6
        let threeResult = mInput7 * 100 * 37.5
        mResult9 = secondResult / threeResult;
        mResult9 = replaceDat(mRes: mResult9.isNaN || mResult9.isInfinite ? 0.0 : mResult9)
        
    }
  
    func replaceDat(mRes : Double)->Double{
           if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
               print("########### Pais Code->",countryCode)
           }
           let aString = mRes.toString()
           let newString = aString.replacingOccurrences(of: ",", with: ".")
           return newString.toDouble()!
    }
    
    func replaceDat14(mRes : Double)->Double{
           if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
               print("########### Pais Code->",countryCode)
           }
           let aString = mRes.toString14()
           let newString = aString.replacingOccurrences(of: ",", with: ".")
           return newString.toDouble()!
    }
}
