//
//  SecondHomeViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/13/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class SecondHomeViewController: BaseViewController {
    @IBOutlet weak var mLinkButton: UIButton!
    var mCounter : Int = 0
    var timer = Timer()
    @IBOutlet weak var mScrollView: UIScrollView!
    
    ///
    ///Tags Binding Traduction
    @IBOutlet weak var mTitleintroductionLabel: UILabel!
    @IBOutlet weak var mDowloadTitleLabel: UILabel!
    @IBOutlet weak var mIntroductionLabel: UILabel!
    ///
    ///
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false;
        mLinkButton.underline()
        self.setNavigationBarItem()
        setTimer()
        self.navigationItem.title = NSLocalizedString("menu_home", comment: "")

    }
    
    @IBAction func mButtonPreview(_ sender: Any) {
        //launchPreview(mPhotoCLicked: "ic_photo5")
    }
    
    func callSlideMenu(){
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.createMenu(mMainViewController: self)
    }
    
    @IBAction func mLoadLinkButton(_ sender: Any) {
        guard let url = URL(string: "https://siemprevirtual.com/tarjetasaventho/") else { return }
        UIApplication.shared.open(url)
    }
    
    func launchPreview(mPhotoCLicked: String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "ImagesVisorViewController") as! ImagesVisorViewController
        v1.mPhotoCLicked = mPhotoCLicked
        MIBlurPopup.show(v1, on: self)
    }

    func setTimer(){
        mCounter = 0
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction(){
        mCounter += 1
        if mCounter == 1 {
            timer.invalidate()
            callSlideMenu()
        }
    }
    
    
}


extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}




extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}
