//
//  ResultFormsViewController.swift
//  orientacionmedica
//
//  Created by Branchbit on 23/03/21.
//  Copyright © 2021 avento. All rights reserved.
//

import UIKit
import Foundation

class ResultFormsViewController: BaseViewController {
    @IBOutlet weak var mResultInput1Label: UILabel!
    @IBOutlet weak var mResultInput2Label: UILabel!
    @IBOutlet weak var mResultInput3Label: UILabel!
    @IBOutlet weak var mResultInput4Label: UILabel!
    @IBOutlet weak var mResultInput5Label: UILabel!
    @IBOutlet weak var mResultInput6Label: UILabel!
    @IBOutlet weak var mResultInput7Label: UILabel!
    @IBOutlet weak var mResultInput8Label: UILabel!
    @IBOutlet weak var mResultInput9Label: UILabel!
    @IBOutlet weak var mResultInput10Label: UILabel!
    
    @IBOutlet weak var mResult12Lavel: UILabel!
    @IBOutlet weak var mResult11Label: UILabel!
    
    var mListResult : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadResults()
    }
    
    func loadResults(){
        mResultInput1Label.text = mListResult[0]
        mResultInput2Label.text = mListResult[1]
        mResultInput3Label.text = mListResult[2] +  NSLocalizedString("mecanica_vent_resul3", comment: "")
        mResultInput4Label.text = mListResult[3]
        mResultInput5Label.text = mListResult[4]
        mResultInput6Label.text = mListResult[5] + NSLocalizedString("mecanica_vent_resul6", comment: "")
        mResultInput7Label.text = mListResult[6] + NSLocalizedString("mecanica_vent_resut7", comment: "")
        mResultInput8Label.text = mListResult[7]
        let mRoundedResult = mListResult[8].toDouble()?.rounded()
        mResultInput9Label.text = (mRoundedResult?.toString())! + NSLocalizedString("mecanica_vent_resul9", comment: "")
        let mRoundRes = mListResult[9].toDouble()?.rounded()
        mResultInput10Label.text =  (mRoundRes?.toString())! + NSLocalizedString("mecanica_vent_resul10", comment: "")
        
        mResult11Label.text = mListResult[10] + NSLocalizedString("mecanica_vent_resut11", comment: "")
        mResult12Lavel.text = mListResult[11] + NSLocalizedString("mecanica_vent_resut12", comment: "")

        
    }
}
