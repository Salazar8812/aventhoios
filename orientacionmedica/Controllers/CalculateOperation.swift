//
//  CalculateOperation.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/26/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class CalculateOperation: NSObject {
   
    /**
     * Fórmula de peso ideal (Pacientes sin SDRA).
     *
     * @param size
     * @return
     */
    func CalculateWeightManOutSDRA(size : Double) -> Double {
        let mc : Double = size * size
        let res : Double = mc * 23
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculateWeightWomanOUTSDRA(size : Double) -> Double {
        let mc : Double = size * size
        let res : Double = mc * 21.5
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    /**
     * Fórmula de peso predicho (Peso Ardsnet para paciente con SDRA)
     *
     * @param size
     * @return
     */
    func CalculateWeightManWithSDRA(size : Double) ->Double {
        let mc : Double = size - 152.4
        let mFirstResult : Double = mc * 0.91
        let res : Double = mFirstResult + 50
    return replaceDat(mRes:res)
    }
    
    func CalculateWeightWomanWithSDRA(size: Double) -> Double{
        let mc : Double = size - 152.4
        let mFirstResult : Double = mc * 0.91
        let res : Double = mFirstResult + 45.5
    return replaceDat(mRes:res)
    }
    
    /**
     * PEEP inicial
     *
     * @param weight
     * @return
     */
    func CalculatePEEPInitial( weight : Double,  meter : Double) -> Double{
        let mc : Double = meter * meter
        let mFirstResult : Double = weight / mc
        let res : Double = mFirstResult.isNaN ? 0.0 : mFirstResult
    return replaceDat(mRes:res)
    }
    
    /**
     * Ajuste de la FR
     *
     * @param fr, pCO2A, pCO2D
     * @return
     */
    
    func AjusteFR(fr : Double, pCO2A : Double, pCO2D : Double) -> Double{
        let mc : Double = fr * pCO2A
        let mFirstResult : Double = mc / pCO2D
        let res : Double = mFirstResult.isNaN ? 0.0 : mFirstResult
        return replaceDat(mRes:res)
    }
    
    /**
     * Fórmulas para obtener el PaCO2 esperado (CO2e)
     *
     * @param HCO
     * @return
     */
    
    func CalculateAcidosis(HCO : Double) -> Double{
        let mFirstResult : Double = HCO * 1.5
        let res : Double = mFirstResult + 8
    return replaceDat(mRes:res)
    }
    
    func CalculateAlcalosis(HCO : Double) -> Double {
        let mFirstResult : Double = HCO * 0.7;
        let res : Double = mFirstResult + 21;
        return replaceDat(mRes:res)
    }
    
    
    func CalculatePresionAlveolar(PIO2: Double, PaCO2 : Double) -> Double{
        let mFirstResult : Double = PIO2 - PaCO2;
        let res : Double = mFirstResult.isNaN ? 0.0 : mFirstResult
    return replaceDat(mRes:res)
    }
    
    
    /**
     * Fórmula para obtener el CO2 deseado
     *
     * @param cO2
     * @param fr
     * @return
     */
    
    func CalculateFR(cO2 : Double, fr: Double, eCO2: Double) -> Double{
        let mFirstResult : Double = fr * cO2;
        let res : Double = mFirstResult / eCO2;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculateVR(cO2: Double, vt: Double, eCO2: Double)-> Double {
        let mFirstResult : Double = vt * cO2;
        let res : Double = mFirstResult / eCO2;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    /**
     * FORMULAS PARA MONITOREO EN VENTILACIÓN MECÁNICA
     *
     * @return
     */
    
    func CalculateDistencionPulmonar(mPresionMeseta: Double, mPEEP: Double) -> Double {
    return replaceDat(mRes:mPresionMeseta - mPEEP)
    }
    
    func CalculatePresionTransaerea(mPresionMaxia: Double, mPresionMeseta: Double) -> Double {
    return replaceDat(mRes:mPresionMaxia - mPresionMeseta)
    }
    
    func CalculateDistensibilidadEstatica(vt: Double,mPmeseta: Double, mPEEP: Double) -> Double {
        let mFirstResult : Double = mPmeseta - mPEEP;
        let res : Double = vt / mFirstResult;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    /**
     * Fórmula en retiro de la ventilación mecánica
     *
     * @return
     */
    
    func CalculatePoderMecanicoVentilatorio(DP: Double,Pmax : Double,FR: Double, Vt : Double) -> Double{
        let mFirstResult : Double = DP / 2;
        let mSecondResult : Double = Pmax - mFirstResult;
        let aaa : Double = Vt * FR * mSecondResult;
        let res : Double = 0.098 * aaa;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    /**
     * Fórmula en retiro de la ventilación mecánica
     *
     * @return
     */
    
    func CalculateRespiracionesRapidasSuperficiales(mFR : Double, mVC: Double) -> Double {
        let mFirstResult : Double = mFR / mVC;
        let res : Double = mFirstResult;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    
    /**
     * Fórmulas para taller gasómetrico.
     *
     * @return
     */
    func CalculatePresionParcialO2A(mEdad : Double) -> Double {
        let mFirstResult : Double = 0.27 * mEdad;
        let res : Double = 104.2 - mFirstResult;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatePresionParcialO2B(mEdad : Double) -> Double {
        let mFirstResult : Double = mEdad / 4;
        let res : Double = 105 - mFirstResult;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatePresionInspiradaO2(mFiO2 : Double, mPB : Double) -> Double {
        let mFirstResult : Double = mPB - 47;
        let res : Double = mFiO2 * mFirstResult;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatePresionAlveolarO2A(mPIO2 : Double, mPaCO2 : Double) -> Double {
        let mFirstRest : Double = mPaCO2 * 1.2;
        let res : Double = mPIO2 / mFirstRest;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatePresionAlveolarO2B(mPIO2 : Double, mPaCO2: Double) -> Double {
        let mFirstRest : Double = mPaCO2 * 1.25;
        let res : Double = mPIO2 - mFirstRest;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    
    func CalculateDiferenciaAlveoloArterialO2(mPAO2 : Double, PaO2: Double) -> Double {
    return replaceDat(mRes:mPAO2 - PaO2)
    }
    
    func CalculateRelacionArterioAlveolarO2(mPAO2 : Double, PaO2 : Double) -> Double {
    return replaceDat(mRes:PaO2 / mPAO2)
    }
    
    func CalculateRelacionOxigenaciónO2(mPAO2 : Double, mFiO2: Double) -> Double {
    return replaceDat(mRes:mPAO2 / mFiO2)
    }
    
    func CalculateRespiratorioO2(PAaO2 : Double, PaO2 : Double) -> Double {
        let mFirstResult : Double = PAaO2 / PaO2;
        let res : Double = mFirstResult;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatOoxigenación(mFiO2 : Double, mPaw : Double, mPaO2 : Double) -> Double {
        let mFirstRes : Double = mFiO2 * mPaw * 100;
        let res : Double = mFirstRes / mPaO2;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculateCortocircuitosIntrapulmonares(mCcO2 : Double, mCaO2: Double, mCvO2: Double) -> Double{
        let mFirstRes: Double = mCcO2 - mCaO2
        let mSecondRes : Double = mCcO2 - mCvO2
        let res : Double = (mFirstRes / mSecondRes)
        let mResult : Double = res * 100
        return replaceDat(mRes:mResult.isNaN ? 0.0 : mResult)
    }
    
    
    //
    // NUEVAS
    func CalculateDistensibilidadPulmonarEstatica(mVt: Double,  mPmeseta : Double, mPEEP: Double) -> Double {
        let mFirstRes : Double = mPmeseta - mPEEP;
        let res : Double = mVt / mFirstRes;
    return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculateDistensibilidadPulmonarDinamica(mVt : Double, mPmax: Double, mPEEP: Double) -> Double {
        let mFirstRes: Double = mPmax - mPEEP;
        let res : Double = mVt / mFirstRes;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculatePresionTransviaAerea(mPmax : Double, mPmeseta: Double) -> Double {
        let mFirstRes : Double = mPmax - mPmeseta;
        let res : Double = mFirstRes;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    func CalculateResistenciaViaAerea(mPmax : Double, mPmeseta : Double, mFlujo : Double) -> Double {
        let mc : Double = mPmax - mPmeseta;
        let mFirstRes : Double = mc / mFlujo;
        let res : Double = mFirstRes * 60;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }
    
    //NUEVAS 18/18

    func CalculateSaO2FiO2( mSaO2: Double, mFiO2 : Double) -> Double {
        let mFirstRes = mSaO2 / mFiO2;
        return replaceDat(mRes:mFirstRes.isNaN ? 0.0 : mFirstRes)
    }

    func CalculateIndiceROX(mSpO2: Double, mFiO2 : Double, mFR : Double) -> Double {
        let mFirstRes = mSpO2 / mFiO2;
        let res = mFirstRes / mFR;
        return replaceDat(mRes:res.isNaN ? 0.0 : res)
    }

    func CalculateVentilatoryRatio(mVolMin : Double, mPaCO2 : Double, mPP : Double) -> Double{
        let mFirstRes = mVolMin * mPaCO2;
        let mSecondRes = mPP * 100 * 31.5;
        let res = mFirstRes / mSecondRes;
        return replaceDat(mRes: res.isNaN ? 0.0 : res)
    }
    func CalculateVentilatoryRatio(mPaCO2 : Double, mPECO2 : Double) -> Double{
        let mFirstRes = mPaCO2 - mPECO2;
        let mSecondRes = mFirstRes / mPaCO2;
        let res = mSecondRes * 100;
        return replaceDat(mRes: res.isNaN ? 0.0 : res)
    }


    func replaceDat(mRes : Double)->Double{
           if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
               print("########### Pais Code->",countryCode)
           }
           let aString = mRes.toString()
           let newString = aString.replacingOccurrences(of: ",", with: ".")
           return newString.toDouble()!
    }
    
    func calcultaeROXHR(mSAO2 : Double, mFiO2 : Double, mFR : Double, mFC: Double)->Double{
        let mFirstRes = mSAO2 / mFiO2
        let mSecondRes = mFirstRes / mFR;
        let mThreeRes = mFC * 100;
        let mFourRes = mSecondRes / mThreeRes;
        let res = mFourRes * 10000;
        return replaceDat(mRes: res.isNaN ? 0.0 : res)
    }
    
    func calcultareHARCORE(mFC : Double, mEDCDG : Double, mPH : Double, mFR : Double, mPAO2FIO2 : Double)-> Double{
        var mFirstRes = 0.0
        var mSecondRes = 0.0
        var mThreeRes = 0.0
        var mFourRes = 0.0
        var mFiveRes = 0.0

                if (mFC < 120){
                     mFirstRes = 0.0
                } else if(mFC >= 120){
                     mFirstRes = 1.0
                }

                if (mEDCDG == 15){
                     mSecondRes = 0
                }else if (mEDCDG == 13 || mEDCDG == 14){
                     mSecondRes = 2
                }else if (mEDCDG == 11 || mEDCDG == 12){
                     mSecondRes = 5
                }else if (mEDCDG < 10){
                     mSecondRes = 10
                }

                if (mPH >= 7.35){
                     mThreeRes = 0
                }else if (mPH >= 7.30 && mPH <= 7.34) {
                     mThreeRes = 2
                }else if (mPH >= 7.25 && mPH <= 7.29) {
                     mThreeRes = 3
                }else if (mPH < 7.25) {
                     mThreeRes = 4
                }

                if (mFR < 30){
                     mFourRes = 0
                }else if (mFR >= 31 && mFR <= 35) {
                     mFourRes = 1
                }else if (mFR >= 36 && mFR <= 40) {
                     mFourRes = 2
                }else if (mFR >= 41 && mFR <= 45) {
                     mFourRes = 3
                }else if (mPH > 45) {
                     mFourRes = 4
                }

                if (mPAO2FIO2 > 200){
                     mFiveRes = 0
                }else if (mPAO2FIO2 >= 176 && mPAO2FIO2 <= 200) {
                     mFiveRes = 2
                }else if (mPAO2FIO2 >= 151 && mPAO2FIO2 <= 175) {
                     mFiveRes = 3
                }else if (mPAO2FIO2 >= 126 && mPAO2FIO2 <= 150) {
                     mFiveRes = 4
                }else if (mPAO2FIO2 >= 101 && mPAO2FIO2 <= 125) {
                     mFiveRes = 5
                }else if (mPAO2FIO2 < 100) {
                     mFiveRes = 6
                }

        let res = mFirstRes + mSecondRes + mThreeRes + mFourRes + mFiveRes
        return replaceDat(mRes: res.isNaN ? 0.0 : res)
    }
    
    func calculateWEBSCORE(mFR : Double,escenario : Int)-> Double{
        var mFirstRes = 0.0
        var mSecondRes = 0.0

                if (mFR <= 20){
                    mFirstRes = 1
                }else if (mFR >= 21 && mFR <= 25) {
                    mFirstRes = 2
                }else if (mFR >= 26 && mFR <= 30) {
                    mFirstRes = 3
                }else if (mFR > 30) {
                    mFirstRes = 4
                }
                if (escenario == 1){
                    mSecondRes = 3
                }else if (escenario == 2){
                    mSecondRes = 1
                }else if (escenario == 3){
                    mSecondRes = 1
                }else if (escenario == 4){
                    mSecondRes = 1
                }else if (escenario == 5){
                    mSecondRes = 2
                }else if (escenario == 6){
                    mSecondRes = 2
                }else if (escenario == 7){
                    mSecondRes = 2
                }else if (escenario == 0){
                    mSecondRes = 0
                }

                let res = mFirstRes + mSecondRes;
        return replaceDat(mRes: res.isNaN ? 0.0 : res)
    }
}
