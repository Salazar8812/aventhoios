//
//  ResultFormsTallerViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 08/04/21.
//  Copyright © 2021 avento. All rights reserved.
//

import UIKit

class ResultFormsTallerViewController: BaseViewController {
    @IBOutlet weak var mResultInput1Label: UILabel!
    @IBOutlet weak var mResultInput2Label: UILabel!
    @IBOutlet weak var mResultInput3Label: UILabel!
    @IBOutlet weak var mResultInput4Label: UILabel!
    @IBOutlet weak var mResultInput5Label: UILabel!
    @IBOutlet weak var mResultInput6Label: UILabel!
    @IBOutlet weak var mResultInput7Label: UILabel!
    @IBOutlet weak var mResultInput8Label: UILabel!
    @IBOutlet weak var mResultInput9Label: UILabel!
    
    var mListResult : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadResults()
    }
    
    
    func loadResults(){
        mResultInput1Label.text = mListResult[0] + NSLocalizedString("taller_gaso_result_driving", comment: "")
        mResultInput2Label.text = mListResult[1] + NSLocalizedString("taller_gaso_result_presion", comment: "")
        mResultInput3Label.text = mListResult[2]
        mResultInput4Label.text = mListResult[3]
        mResultInput5Label.text = mListResult[4]
        mResultInput6Label.text = mListResult[5]
        mResultInput7Label.text = mListResult[6]
        mResultInput8Label.text = mListResult[7] + NSLocalizedString("taller_gaso_result_poder", comment: "")
        mResultInput9Label.text = mListResult[8] + NSLocalizedString("taller_gaso_result_ratio", comment: "")
    }

}
