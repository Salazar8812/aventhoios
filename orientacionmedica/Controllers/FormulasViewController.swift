//
//  FormulasViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class FormulasViewController: BaseViewController,TableViewCellClickDelegate {
    var mDataSource : BaseDataSource<NSObject, ItemFormulaBaseTableViewCell>?
    var mListFomulas : [Formulas] = []
    @IBOutlet weak var mSearchImageView: UIImageView!
    
    @IBOutlet weak var mFormulasTableView: UITableView!
    
    @IBOutlet weak var mSearchTextField: UITextField!
    
    var mListAux : [Formulas] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateFormulas()
        mDataSource = BaseDataSource(tableView: self.mFormulasTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mListFomulas)
        
        setTintcolor()
        
        addTarget()
        
        addFotter()
    }
    
    func addFotter(){
        let view = UIView(frame: CGRectMake(0,0, mFormulasTableView.frame.size.width, 120))
        mFormulasTableView.tableFooterView = view
    }
    
    func addTarget (){
        mSearchTextField.addTarget(self, action: #selector(watcher(_:)), for: .editingChanged)
    }
    
    func setTintcolor(){
        mSearchImageView.image = mSearchImageView.image?.withRenderingMode(.alwaysTemplate)
        //mSearchImageView.tintColor = UIColor(netHex: Colors.color_background_app)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "Formulas"
        mSearchTextField.text = ""
        mDataSource!.update(items: mListFomulas)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FormsCalculateViewController") as! FormsCalculateViewController
        loginVC.mFormulas = (item as! Formulas)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @objc func watcher(_ textfield : UITextField){
        var cadAux : String = ""
        cadAux = textfield.text!
        mListAux.removeAll()
        if(cadAux.count == 0 || cadAux == ""){
            mDataSource!.update(items: mListFomulas)
        }else{
            for item in mListFomulas{
                if(item.mTipoFormula.uppercased().contains(cadAux.uppercased())){
                    mListAux.append(item)
                }
            }
            mDataSource!.update(items: mListAux)
        }
    }
    
    func populateFormulas(){
        //mListFomulas.append(Formulas(mTipoFormula: "SaO2/FiO2", mNombreFormula: "SaO2/FiO2.", mIdFormula: "1",mParams: ["SaO2 (%) *", "FiO2 en decimales (ej. 0.21) *"],mFunctionCalculate: "CalculateSaO2FiO2", mDescription: StringDescriptions.description_form_empty))
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_indice_rox", comment: ""), mNombreFormula: NSLocalizedString("description_form_rox", comment: ""), mIdFormula: "1",mParams:[NSLocalizedString("form_rox_spo2", comment: ""), NSLocalizedString("form_rox_fio2", comment: ""), NSLocalizedString("form_rox_fr", comment: "")],mFunctionCalculate: "CalculateIndiceROX", mDescription: StringDescriptions.description_form_empty))
        
        //mListFomulas.append(Formulas(mTipoFormula: "Ventilatory Ratio", mNombreFormula: "Ventilatory Ratio.", mIdFormula: "1",mParams:["Volumen minuto (ml) *", "paCO2 *", "Peso Predicho *"],mFunctionCalculate: "CalculateVentilatoryRatio", mDescription: StringDescriptions.description_form_empty))
        
        mListFomulas.append(Formulas(mTipoFormula:NSLocalizedString("name_form_indice_ecuacion_bohr", comment: ""), mNombreFormula: NSLocalizedString("description_form_ecuacion_bohr", comment: ""), mIdFormula: "1",mParams:["PaCO2 *", "PECO2 *"],mFunctionCalculate: "CalculateEspacioMuerto", mDescription: StringDescriptions.description_form_empty))
        
        if(!mTranslateLanguage.isLanguageUS()){
            mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_peso_ideal", comment: ""), mNombreFormula: NSLocalizedString("description_form_peso_ideal", comment: ""), mIdFormula: "1", mParams: ["Talla (cm)2 *"],mFunctionCalculate: "CalculateWeightManOutSDRA", mDescription: StringDescriptions.description_form12));
        }
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_peso_predicho", comment: ""), mNombreFormula: NSLocalizedString("description_form_peso_prdicho", comment: ""), mIdFormula: "1",mParams: [NSLocalizedString("form_sdra", comment: "")],mFunctionCalculate: "CalculateWeightManWithSDRA",mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_peep_inicial", comment: ""), mNombreFormula: NSLocalizedString("description_form_peep_inicial", comment: ""), mIdFormula: "1",mParams: [NSLocalizedString("form_imc", comment: ""), "(m)2 *"],mFunctionCalculate: "CalculatePEEPInitial",mDescription: StringDescriptions.description_form_empty));
        
        //nuevas
        
       /* mListFomulas.append(Formulas(mTipoFormula: "", mNombreFormula: "Distensibilidad pulmonar estática (Cst compliance estática)", mIdFormula: "1",mParams: ["Vt (ml) *","Pmeseta *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarEstatica", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "", mNombreFormula: "Distensibilidad pulmonar dinámica (Cdyn compliance dinámica)", mIdFormula: "1",mParams: ["Vt (ml) *","Pmax *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarDinamica", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "", mNombreFormula: "Presión trans-vía aerea", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *"],mFunctionCalculate: "CalculatePresionTransviaAerea", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "", mNombreFormula: "Resistencia de la vía aerea (Raw)", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *", "Flujo *"],mFunctionCalculate: "CalculateResistenciaViaAerea", mDescription: StringDescriptions.description_form_empty));*/
        
        mListFomulas.append(Formulas(mTipoFormula:NSLocalizedString("name_form_correccion_co2", comment: ""), mNombreFormula: NSLocalizedString("description_form_acidiosis", comment: ""), mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAcidosis",mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_correccion_co2", comment: ""), mNombreFormula: NSLocalizedString("description_form_alcalosis", comment: ""), mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAlcalosis",mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_correccion_co2", comment: ""), mNombreFormula: NSLocalizedString("description_form_ajust_fr", comment: ""), mIdFormula: "1",mParams: [NSLocalizedString("form_rox_fr", comment: ""),"paCO2 *","CO2e *"],mFunctionCalculate: "CalculateFR", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_rox_hr", comment: ""), mNombreFormula: NSLocalizedString("description_form_rox_hr", comment: ""), mIdFormula: "1",mParams: ["Sa O2 (%) *",NSLocalizedString("form_fio2", comment: ""),NSLocalizedString("form_rox_fr", comment: ""), NSLocalizedString("form_fc", comment: "")],mFunctionCalculate: "CalculateROX", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_hacor_score", comment: ""), mNombreFormula: NSLocalizedString("description_form_hacor_score", comment: ""), mIdFormula: "1",mParams: ["HR *","Escala de coma Glasow ","pH *",NSLocalizedString("form_rox_fr", comment: ""),"paO2/fiO2"],mFunctionCalculate: "CalculateHARCORE", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_wob_score", comment: ""), mNombreFormula:NSLocalizedString("description_form_wob_score", comment: ""), mIdFormula: "1",mParams: [NSLocalizedString("form_rox_fr", comment: "")],mFunctionCalculate: "CalculateWOBSCORE", mDescription: StringDescriptions.description_form_empty));
        
        /*********/
        
        /*mListFomulas.append(Formulas(mTipoFormula: "Monitoreo de Presiones de Ventilación Mecánica asisto-controlada por Volumen", mNombreFormula: "Presión de distensión pulmonar (Driving pressure)", mIdFormula: "1",mParams: ["Presión meseta *","PEEP *"],mFunctionCalculate: "CalculateDistencionPulmonar", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Poder mecánico ventilatorio", mNombreFormula: "Poder mecánico ventilatorio", mIdFormula: "1",mParams: ["Driving pressure *","Pmax *", "FR *" , "VT (lt) *"],mFunctionCalculate: "CalculatePoderMecanicoVentilatorio", mDescription: StringDescriptions.description_form_empty));
        //listaUsuarios.add(new Formulas("Taller gasométrico respiratorio", "Presión parcial de O2 Arterial (PaO2)", "1",new String[]{"Edad en años *"},"CalculatePresionParcialO2A", getString(R.string.description_form_empty)));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Presión parcial de O2 Arterial (PaO2)", mIdFormula: "1",mParams: ["¼ edad en años *"],mFunctionCalculate: "CalculatePresionParcialO2B", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Presión Inspirada de O2 (PIO2)", mIdFormula: "1",mParams: ["FiO2 (en decimales, máximo 1) *","PB *","PH2O *"],mFunctionCalculate: "CalculatePresionInspiradaO2", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Presión Alveolar de O2 (PAO2)", mIdFormula: "1",mParams: ["PIO2 *","PaCO2 *"],mFunctionCalculate: "CalculatePresionAlveolar", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Presión Alveolar de O2 (PAO2)\nSi el FiO2 es > 60%, usar esta fórmula", mIdFormula: "1",mParams: ["PIO2 *","PaCO2 *"],mFunctionCalculate: "CalculatePresionAlveolarO2B", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Diferencia Alveolo-arterial de O2 [P (A-a)O2]\nNormal 10 a 15 mm HG", mIdFormula: "1",mParams: ["PAO2 *","PaO2 *"],mFunctionCalculate: "CalculateDiferenciaAlveoloArterialO2", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Relación Arterio-alveolar de O2 [P (a/A)O2]", mIdFormula: "1",mParams: ["PaO2 *","PAO2 *"],mFunctionCalculate: "CalculateRelacionArterioAlveolarO2", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Índice de Kirby", mIdFormula: "1",mParams: ["PaO2 *","FiO2 (en decimales, máximo 1)*"],mFunctionCalculate: "CalculateRelacionOxigenaciónO2", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Índice Respiratorio (IR)", mIdFormula: "1",mParams: ["P(A-a)O2 *","PaO2 *"],mFunctionCalculate: "CalculateRespiratorioO2", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Índice de oxigenación (IO))", mIdFormula: "1",mParams: ["FiO2 (en decimales, máximo 1) *","Paw *","PaO2 *"],mFunctionCalculate: "CalculatOoxigenación", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Taller gasométrico respiratorio", mNombreFormula: "Cortocircuitos Intrapulmonares (Qs / Qt)", mIdFormula: "1",mParams: ["CcO2 *","CaO2 *","CvO2 *"],mFunctionCalculate: "CalculateCortocircuitosIntrapulmonares", mDescription: StringDescriptions.description_form_empty));
        
        mListFomulas.append(Formulas(mTipoFormula: "Predictores para el éxito al retiro de la ventilación mecánica", mNombreFormula: "Índice de Respiraciones Rápidas Superficiales\nVolumen corriente en Litros", mIdFormula: "1",mParams: ["FR *","VC (lt) *"],mFunctionCalculate: "CalculateRespiracionesRapidasSuperficiales", mDescription: StringDescriptions.description_form_empty));*/
        
        
    }
    
}
