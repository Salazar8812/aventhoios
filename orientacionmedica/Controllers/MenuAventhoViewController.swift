//
//  MenuAventhoViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 06/02/20.
//  Copyright © 2020 avento. All rights reserved.
//

import UIKit

class MenuAventhoViewController: BaseViewController {
    var viewController: UIViewController!
    @IBOutlet weak var mHomeImageView: UIImageView!
    @IBOutlet weak var mFormImageView: UIImageView!
    @IBOutlet weak var mConceptosImageView: UIImageView!
    @IBOutlet weak var mContactImageView: UIImageView!
    
    var mHome : UIViewController!
    var mForms : UIViewController!
    var mConcept : UIViewController!
    var mContact : UIViewController!
    
    @IBOutlet weak var mBannerTitle: UIView!
    @IBOutlet weak var mHeightContraint: NSLayoutConstraint!
    
    /**
     Binding View
     */
    
    @IBOutlet weak var mTitleMenuLabel: UILabel!
    @IBOutlet weak var mMenuHomeLabel: UILabel!
    @IBOutlet weak var mMenuContactLabel: UILabel!
    @IBOutlet weak var mMenuFormulasLabel: UILabel!
    @IBOutlet weak var mMenuConceptLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addViewControllers()
        mConceptosImageView.image = mConceptosImageView.image?.withRenderingMode(.alwaysTemplate)
        mConceptosImageView.tintColor = UIColor(netHex: Colors.color_gray_menu)
    }
    
    
    func addViewControllers(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home = storyboard.instantiateViewController(withIdentifier: "SecondHomeViewController") as! SecondHomeViewController
        self.mHome = UINavigationController(rootViewController: home)
        
        let formula = storyboard.instantiateViewController(withIdentifier: "FormulasTabViewController") as! FormulasTabViewController
        self.mForms = UINavigationController(rootViewController: formula)
        
        let conceptos = storyboard.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        self.mConcept = UINavigationController(rootViewController: conceptos)
        
        let contacto = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.mContact = UINavigationController(rootViewController: contacto)
        
    }
    
    
    
    @IBAction func mHomeButton(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(mHome, close: true)
    }
    
    @IBAction func mFormButton(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(mForms, close: true)
    }
    
    @IBAction func mConceptButton(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(mConcept, close: true)
    }
    
    @IBAction func mContactButton(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(mContact, close: true)
    }
    
}
