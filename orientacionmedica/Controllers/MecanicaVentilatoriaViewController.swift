//
//  MecanicaVentilatoriaViewController.swift
//  orientacionmedica
//
//  Created by Branchbit on 19/03/21.
//  Copyright © 2021 avento. All rights reserved.
//

import UIKit

class MecanicaVentilatoriaViewController: BaseViewController {
    @IBOutlet weak var mInput1TextField: UITextField!
    @IBOutlet weak var mInput2TextField: UITextField!
    @IBOutlet weak var mInput3TextField: UITextField!
    @IBOutlet weak var mInput4TextField: UITextField!
    @IBOutlet weak var mInput5TextField: UITextField!
    @IBOutlet weak var mInput6TextField: UITextField!
    @IBOutlet weak var mInput7TextField: UITextField!
    @IBOutlet weak var mInput8TextField: UITextField!
    @IBOutlet weak var mCalcularButton: UIButton!
    
    @IBOutlet weak var mPVOTextField: UITextField!
    
    
    var mResult1 : Double = 0.0
    var mResult2 : Double = 0.0
    var mResult3 : Double = 0.0
    var mResult4 : Double = 0.0
    var mResult5 : Double = 0.0
    var mResult6 : Double = 0.0
    var mResult7 : Double = 0.0
    var mResult8 : Double = 0.0
    var mResult9 : Double = 0.0
    var mResult10 : Double = 0.0
    var mResult11 : Double = 0.0
    var mResult12 : Double = 0.0
    
    var mRes2 : Double = 0.0
    var mRes4 : Double = 0.0
    var mRes5 : Double = 0.0

    @IBOutlet weak var mScrollView: UIScrollView!
    
    
    var mInput1 : Double = 0.0
    var mInput2 : Double = 0.0
    var mInput3 : Double = 0.0
    var mInput4 : Double = 0.0
    var mInput5 : Double = 0.0
    var mInput6 : Double = 0.0
    var mInput7 : Double = 0.0
    var mInput8 : Double = 0.0
    var mInput9 : Double = 0.0
    
    var mListField : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addTargetButton()
        addFields()
        setData()
    }
    
    func addTargetButton(){
        mCalcularButton.addTarget(self, action: #selector(setter: mCalcularButton), for: .touchUpInside)
    }


    
    @IBAction func mCalcResButton(_ sender: Any) {
        print("Click")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResultFormsViewController") as! ResultFormsViewController
        setData()
        executeForms()
        vc.mListResult.append(mResult1.toString())
        vc.mListResult.append(mResult2.toString())
        vc.mListResult.append(mResult3.toString())
        vc.mListResult.append(mResult4.toString())
        vc.mListResult.append(mResult5.toString())
        vc.mListResult.append(mResult6.toString())
        vc.mListResult.append(mResult7.toString())
        vc.mListResult.append(mResult8.toString())
        vc.mListResult.append(mResult9.toString())
        vc.mListResult.append(mResult10.toString())
        vc.mListResult.append(mResult11.toString())
        vc.mListResult.append(mResult12.toString())
        self.present(vc, animated: true, completion: nil)
    }
    
    func addFields(){
        mListField.append(mInput1TextField)
        mListField.append(mInput2TextField)
        mListField.append(mInput3TextField)
        mListField.append(mInput4TextField)
        mListField.append(mInput5TextField)
        mListField.append(mInput6TextField)
        mListField.append(mInput7TextField)
        mListField.append(mInput8TextField)
    }
    
    func executeForms(){
        form1()
        form2()
        form3()
        form4()
        form5()
        form6()
        form7()
        form8()
        form9()
        form10()
        form11()
        form12()
    }
    
    func setData(){
        mInput1 = mInput1TextField.text != "" ? mInput1TextField.text!.toDouble()! : 0.0
        mInput2 = mInput2TextField.text != "" ? mInput2TextField.text!.toDouble()! : 0.0
        mInput3 = mInput3TextField.text != "" ? mInput3TextField.text!.toDouble()! : 0.0
        mInput4 = mInput4TextField.text != "" ? mInput4TextField.text!.toDouble()! : 0.0
        mInput5 = mInput5TextField.text != "" ? mInput5TextField.text!.toDouble()! : 0.0
        mInput6 = mInput6TextField.text != "" ? mInput6TextField.text!.toDouble()! : 0.0
        mInput7 = mInput7TextField.text != "" ? mInput7TextField.text!.toDouble()! : 0.0
        mInput8 = mInput8TextField.text != "" ? mInput8TextField.text!.toDouble()! : 0.0
        mInput9 = mPVOTextField.text != "" ? mPVOTextField.text!.toDouble()! : 0.0
    }
    
    func form1(){
        if (mInput1 == 0.0){
            mResult1 = 0.0
        }else{
            let firstResult = (mInput1) - 47
            let secondResult = (mInput2) / 100
            //let secondResult = firstResult * 0.21
            let threeResult = firstResult * secondResult;
            mResult1 = threeResult
            mResult1 = replaceDat(mRes: mResult1.isNaN || mResult1.isInfinite ? 0.0 : mResult1)
        }
        
    }
    
    func form2(){
        if(mInput4 == 0.0){
            mResult2 = 0.0
        }else{
            let firstResult =  mInput4 / 0.8 //mResult1 - (mInput4) / 0.8
            mResult2 = mResult1 - firstResult
            mRes2 = mResult2
            mResult2 = replaceDat(mRes: mResult2.isNaN || mResult2.isInfinite ? 0.0 : mResult2)
        }
       
    }
    
    func form3(){
        if(mInput3 == 0.0){
            mResult3 = 0.0
        }else{
            let firstResult = mResult2 - (mInput3)
            mResult3 = firstResult
            mResult3 = replaceDat(mRes: mResult3.isNaN || mResult3.isInfinite ? 0.0 : mResult3)
        }
    }
    
    func form4(){
        let firstResult = (mInput8) * 1.34 * (mInput6)
        let secondResult = (mInput3) * 0.0031
        mResult4 = firstResult + secondResult;
        
        mRes4 = mResult4

        mResult4 = replaceDat(mRes: mResult4.isNaN || mResult4.isInfinite ? 0.0 : mResult4)
    }
    
    func form5(){
        let firstResult = (mInput8) * 1.34 * (mInput7)
        //let secondResult = (mInput5) * 0.0031
        
        let secondResult = (mInput9) * 0.0031
        
        mResult5 = firstResult + secondResult;
        
        mRes5 = mResult5

        mResult5 = replaceDat(mRes: mResult5.isNaN || mResult5.isInfinite ? 0.0 : mResult5)
    }
    
    func form6(){
       /* mResult6 = mResult4 / mResult5;
        mResult6 = replaceDat(mRes: mResult6.isNaN || mResult6.isInfinite ? 0.0 : mResult6)*/
        
        let firstResult = (mInput8 * 1.34) + (mRes2 * 0.0031);
        let secondResult = firstResult - mRes4;
        let threeResult = firstResult - mRes5;
        mResult6 = (secondResult / threeResult)
        print("REsultado " , secondResult)
        print("REsultado " , threeResult)

        print("REsultado " , mResult6)
        mResult6 = mResult6 * 100;
        print("REsultado " , mResult6)

        mResult6 = replaceDat(mRes: mResult6.isNaN || mResult6.isInfinite ? 0.0 : mResult6)
    }
    
    func form7(){
        /*let firstResult = (mInput4) / 0.8
        let secondResult = mResult1 - firstResult
        mResult7 = (mInput3) / secondResult
        mResult7 = replaceDat(mRes: mResult7.isNaN || mResult7.isInfinite  ? 0.0 : mResult7)*/
        let firstResult = mInput3 / mResult2;
        mResult7 = firstResult;
        mResult7 = replaceDat(mRes: mResult7.isNaN || mResult7.isInfinite  ? 0.0 : mResult7)
    }
    
    func form8(){
        /*let firstResult = (mInput4) / 0.8;
        let secondResult = mResult1 - firstResult
        let threeResult = secondResult - (mInput3)
        mResult8 = threeResult / (mInput3)
        mResult8 = replaceDat(mRes: mResult8.isNaN || mResult8.isInfinite ? 0.0 : mResult8)*/
        
        
        let firstResult = mResult3 / mInput3
        mResult8 = firstResult;
        mResult8 = replaceDat(mRes: mResult8.isNaN || mResult8.isInfinite ? 0.0 : mResult8)
    }
    
    func form9(){
        /*let firstResult = (mInput3) / (mInput2)
        mResult9 = firstResult
        mResult9 = replaceDat(mRes: mResult9.isNaN || mResult9.isInfinite ? 0.0 : mResult9)*/
        
        
        let firstResult = mInput2 / 100;
        mResult9 = mInput3 / firstResult;
        mResult9 = replaceDat(mRes: mResult9.isNaN || mResult9.isInfinite ? 0.0 : mResult9)
    }

    func form10(){
        /*let firstResult = (mInput6) / (mInput2);
        mResult10 = firstResult * 100;
        mResult10 = replaceDat(mRes: mResult10.isNaN || mResult10.isInfinite ? 0.0 : mResult10)*/
        
        
        let firstResult = mInput2 / 100;
        let secondResult = mInput6 * 100;
        mResult10 =  secondResult / firstResult;
        mResult10 = replaceDat(mRes: mResult10.isNaN || mResult10.isInfinite ? 0.0 : mResult10)
    }
    
    func form11() {
        let firstResult = mInput5 - mInput4
        mResult11 = firstResult
        mResult11 = replaceDat(mRes: mResult11.isNaN || mResult11.isInfinite ? 0.0 : mResult11)
     }

    func form12() {
        let firstResult = mInput6 - mInput7
        let secondResult = firstResult / mInput6
        mResult12 = secondResult * 100;
        mResult12 = replaceDat(mRes: mResult12.isNaN || mResult12.isInfinite ? 0.0 : mResult12)
     }
    
    func replaceDat(mRes : Double)->Double{
           if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
               print("########### Pais Code->",countryCode)
           }
           let aString = mRes.toString()
           let newString = aString.replacingOccurrences(of: ",", with: ".")
           return newString.toDouble()!
    }
}
