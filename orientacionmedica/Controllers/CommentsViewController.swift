//
//  CommentsViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class CommentsViewController: BaseViewController, TableViewCellClickDelegate {
    
    @IBOutlet weak var mSearchImageView: UIImageView!
    @IBOutlet weak var mListConceptTableView: UITableView!
    @IBOutlet weak var mSearchTextField: UITextField!
    var mDataSource : BaseDataSource<NSObject, ItemConceptTableViewCell>?
    var mListFomulas : [Formulas] = []
    var mListAux : [Formulas] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateFormulas()
        mDataSource = BaseDataSource(tableView: self.mListConceptTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mListFomulas)
        
        addTarget()
        setTintcolor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = NSLocalizedString("menu_conceptos", comment: "")
        mSearchTextField.text = ""
        mDataSource?.update(items: mListFomulas)
    }
    
    func setTintcolor(){
        mSearchImageView.image = mSearchImageView.image?.withRenderingMode(.alwaysTemplate)
        //mSearchImageView.tintColor = UIColor(netHex: Colors.color_background_app)
    }
    
    func addTarget(){
        mSearchTextField.addTarget(self, action: #selector(watcher(_:)), for: .editingChanged)
    }
    
    @objc func watcher(_ textfield : UITextField){
        var cadAux : String = ""
        cadAux = textfield.text!
        mListAux.removeAll()
        if(cadAux.count == 0 || cadAux == ""){
            mDataSource!.update(items: mListFomulas)
        }else{
            for item in mListFomulas{
                if(item.mTipoFormula.uppercased().contains(cadAux.uppercased())){
                    mListAux.append(item)
                }
            }
            mDataSource!.update(items: mListAux)
        }
    }
    
    func populateFormulas(){
        if(!mTranslateLanguage.isLanguageUS()){
            mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_peso_ideal", comment: ""), mNombreFormula: NSLocalizedString("description_form_peso_ideal", comment: ""), mIdFormula: "1", mParams: ["Talla (cm)2 *"],mFunctionCalculate: "CalculateWeightManOutSDRA", mDescription: StringDescriptions.description_form2));
        }
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("description_form_peso_prdicho", comment: ""), mNombreFormula: NSLocalizedString("description_form_peso_prdicho", comment: ""), mIdFormula: "1",mParams: ["Talla (cm) *"],mFunctionCalculate: "CalculateWeightManWithSDRA",mDescription: StringDescriptions.description_form3));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_peep_inicial", comment: ""), mNombreFormula: NSLocalizedString("description_form_peep_inicial", comment: ""), mIdFormula: "1",mParams: ["Peso (Kg) *", "(m)2 *"],mFunctionCalculate: "CalculatePEEPInitial",mDescription: StringDescriptions.description_form4));
        
        //nuevas
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_correccion_co2", comment: ""), mNombreFormula: NSLocalizedString("description_form_acidiosis", comment: ""), mIdFormula: "1",mParams: ["Vt (ml) *","Pmeseta *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarEstatica", mDescription: StringDescriptions.description_form5));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_presion_baro", comment: ""), mNombreFormula: "Distensibilidad pulmonar dinámica (Cdyn compliance dinámica)", mIdFormula: "1",mParams: ["Vt (ml) *","Pmax *","PEEP *"],mFunctionCalculate: "CalculateDistensibilidadPulmonarDinamica", mDescription: StringDescriptions.description_form6));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_presion_vapor", comment: ""), mNombreFormula: NSLocalizedString("name_form_presion_vapor", comment: ""), mIdFormula: "1",mParams: ["Pmax *","Pmeseta *"],mFunctionCalculate: "CalculatePresionTransviaAerea", mDescription: StringDescriptions.description_form7));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_presion_parcial", comment: ""), mNombreFormula: NSLocalizedString("name_form_presion_parcial", comment: ""), mIdFormula: "1",mParams: ["Pmax *","Pmeseta *", "Flujo *"],mFunctionCalculate: "CalculateResistenciaViaAerea", mDescription: StringDescriptions.description_form8));
        
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_presion_inspi", comment: ""), mNombreFormula: NSLocalizedString("name_form_presion_inspi", comment: ""), mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAcidosis",mDescription: StringDescriptions.description_form9));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_presion_alveolar", comment: ""), mNombreFormula: NSLocalizedString("name_form_presion_alveolar", comment: ""), mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "CalculateAlcalosis",mDescription: StringDescriptions.description_form10));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_dif_alveolar", comment: ""), mNombreFormula: NSLocalizedString("name_form_dif_alveolar", comment: ""), mIdFormula: "1",mParams: ["Fr *","paCO2 *","CO2e *"],mFunctionCalculate: "CalculateFR", mDescription: StringDescriptions.description_form11));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_corto_intra", comment: ""), mNombreFormula: NSLocalizedString("name_form_corto_intra", comment: ""), mIdFormula: "1",mParams: ["Presión meseta *","PEEP *"],mFunctionCalculate: "CalculateDistencionPulmonar", mDescription: StringDescriptions.description_form12));
        mListFomulas.append(Formulas(mTipoFormula: NSLocalizedString("name_form_poder_meca", comment: ""), mNombreFormula: NSLocalizedString("name_form_poder_meca", comment: ""), mIdFormula: "1",mParams: ["Driving pressure *","Pmax *", "FR *" , "VT (lt) *"],mFunctionCalculate: "CalculatePoderMecanicoVentilatorio", mDescription: StringDescriptions.description_form13));
        //listaUsuarios.add(new Formulas("Taller gasométrico respiratorio", "Presión parcial de O2 Arterial (PaO2)", "1",new String[]{"Edad en años *"},"CalculatePresionParcialO2A", getString(R.string.description_form_empty)));
       
        
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let mFormula = item as! Formulas
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "DescriptionFormulasViewController") as! DescriptionFormulasViewController
        v1.mTitle = mFormula.mTipoFormula
        v1.mContentDescription = mFormula.mDescription
        MIBlurPopup.show(v1, on: self)
    }

}
