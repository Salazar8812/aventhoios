//
//  FormulasTabViewController.swift
//  orientacionmedica
//
//  Created by Branchbit on 19/03/21.
//  Copyright © 2021 avento. All rights reserved.
//

import UIKit

class FormulasTabViewController: BaseViewController {
    @IBOutlet weak var mMainContainerView: UIView!
    var mArrayControllers : [UIViewController] = []
    var mArrayTitles : [String] = []
    var mCounter = 0
    var mTimer = Timer()
    var slidingContainerViewController : SlidingContainerViewController?
    var mMainController : UIViewController?
    @IBOutlet weak var mTabMecanicaView: UIView!
    @IBOutlet weak var mTallerButtonç: UIButton!
    @IBOutlet weak var mTabMecanicaButton: UIButton!
    @IBOutlet weak var mTabTallerView: UIView!
    
    @IBOutlet weak var mTallerGasView: UIView!
    @IBOutlet weak var mTallerGasButton: UIButton!
    
    @IBOutlet weak var mFormulasLabel: UILabel!
    @IBOutlet weak var mMecanicaLabel: UILabel!
    @IBOutlet weak var mTallerLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("menu_home", comment: "")
        self.automaticallyAdjustsScrollViewInsets = false;
        self.setNavigationBarItem()
        addTarget()
        delayLaunchIntroductionView()
    }
    
    func delayLaunchIntroductionView(){
           mTimer.invalidate()
           mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelay), userInfo: nil, repeats: true)
    }
       
    @objc func actionDelay() {
        mCounter += 1
        if(mCounter == 1){
            mTimer.invalidate()
            createChildFormulas()
        }
    }
       
    func addTarget(){
        mTallerButtonç.addTarget(self, action: #selector(actionTallerTab), for: .touchUpInside)
        
        mTabMecanicaButton.addTarget(self, action: #selector(actionMecanicaTab), for: .touchUpInside)

    }
    
    @objc func actionTallerTab(){
        createChildViewMecanica()
        setSelectedTab(mSelectorTab: mTabTallerView, mButton: mMecanicaLabel, mColor: UIColor(netHex: Colors.color_blue_app), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: true)
        
        setSelectedTab(mSelectorTab: mTallerGasView, mButton: mTallerLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
        
        setSelectedTab(mSelectorTab: mTabMecanicaView, mButton: mFormulasLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
    }
    
    @objc func actionMecanicaTab(){
        createChildFormulas()
        
        setSelectedTab(mSelectorTab: mTabTallerView, mButton: mMecanicaLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
        
        setSelectedTab(mSelectorTab: mTallerGasView, mButton: mTallerLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
        
        setSelectedTab(mSelectorTab: mTabMecanicaView, mButton: mFormulasLabel, mColor: UIColor(netHex: Colors.color_blue_app), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: true)
    }
    

    
    func setSelectedTab(mSelectorTab: UIView, mButton : UILabel, mColor: UIColor, mColorButton : UIColor, mSelected : Bool){
        mSelectorTab.backgroundColor = mColor
        mButton.textColor = mColorButton
        if(mSelected){
            mButton.font = UIFont.boldSystemFont(ofSize: 16)
        }else{
            mButton.font = UIFont.systemFont(ofSize: 15)
        }
    }
    
    func setDataSourceTabsController(){
        slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: [MecanicaVentilatoriaViewController(),TallerGasometricoViewController()],
            titles: [NSLocalizedString("fragment_two_tittle_2", comment: ""),NSLocalizedString("fragment_two_tittle_3", comment: "")])
        mMainContainerView.addSubview((slidingContainerViewController?.view)!)
        addChildViewController(slidingContainerViewController!)
        slidingContainerViewController!.didMove(toParentViewController: self)
    }
    
    @IBAction func mTallerAction(_ sender: Any) {
        createChildViewTaller()
        setSelectedTab(mSelectorTab: mTabTallerView, mButton: mMecanicaLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
        
        setSelectedTab(mSelectorTab: mTallerGasView, mButton: mTallerLabel, mColor: UIColor(netHex: Colors.color_blue_app), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: true)
        
        setSelectedTab(mSelectorTab: mTabMecanicaView, mButton: mFormulasLabel, mColor: UIColor(netHex: Colors.color_white), mColorButton: UIColor(netHex: Colors.color_blue_app), mSelected: false)
        
    }
    
    func createChildViewMecanica(){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "TallerGasometricoViewController") as! TallerGasometricoViewController)
        mMainContainerView.addSubview(mMainController!.view)
        addChildViewController(mMainController!)
        mMainController!.didMove(toParentViewController: self)
    }
    
    func createChildFormulas(){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "FormulasViewController") as! FormulasViewController)
        mMainContainerView.addSubview(mMainController!.view)
        addChildViewController(mMainController!)
        mMainController!.didMove(toParentViewController: self)
    }
    
    
    func createChildViewTaller(){
        mMainController = (self.storyboard?.instantiateViewController(withIdentifier: "MecanicaVentilatoriaViewController") as! MecanicaVentilatoriaViewController)
        mMainContainerView.addSubview(mMainController!.view)
        addChildViewController(mMainController!)
        mMainController!.didMove(toParentViewController: self)
    }
    
}
