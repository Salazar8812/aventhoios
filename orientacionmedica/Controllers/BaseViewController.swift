//
//  BaseViewController.swift
//  orientacionmedica
//
//  Created by Carlos Salazar Vazquez on 31/01/23.
//  Copyright © 2023 avento. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var mArrarViewsTraduction : [ElementsTranslate] = []
    var mTranslateLanguage : LanguageLocalized!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTranslateLanguage = LanguageLocalized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            let statusBar = UIView(frame: (keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = UIColor(netHex: Colors.color_background_app)
            keyWindow?.addSubview(statusBar)
            navigationController?.navigationBar.barStyle = .black
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func scanListViews(){
        for mitem in mArrarViewsTraduction{
            detectElement(mElement: mitem)
        }
    }
    
    func detectElement(mElement : ElementsTranslate){
        if(mElement.mType == .boton){
            transformButton(mView: mElement.mElement!).setTitle(mElement.mText, for: .normal)
        }else{
            transformLabel(mView: mElement.mElement!).text = mElement.mText
        }
    }
    
    func transformButton(mView : UIView)->UIButton{
        return mView as! UIButton
    }
    
    func transformLabel(mView : UIView)->UILabel{
        return mView as! UILabel
    }
    
   
}

