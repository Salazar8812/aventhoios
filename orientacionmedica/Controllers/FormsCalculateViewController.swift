//
//  FormsCalculateViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/11/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import DLRadioButton

class FormsCalculateViewController: UIViewController {
    var mResult : Double = 0.0
    var formSelected : String = ""
    var mFormulas : Formulas!
    @IBOutlet weak var mContentFieldThree: UIView!
    @IBOutlet weak var mDetailFormula: UILabel!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mHeightConstraintFour: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintThree: NSLayoutConstraint!
    @IBOutlet weak var mHeightConstraintTwo: NSLayoutConstraint!
    @IBOutlet weak var act_form_name: UILabel!
    @IBOutlet weak var mContentFieldFour: UIView!
    @IBOutlet weak var mContentFieldTwo: UIView!
    @IBOutlet weak var mEditFieldOne: UITextField!
    @IBOutlet weak var mEditFieldThree: UITextField!
    @IBOutlet weak var mEditFieldFour: UITextField!
    @IBOutlet weak var mFieldOne: UILabel!
    @IBOutlet weak var mEditFieldTwo: UITextField!
    @IBOutlet weak var mContentFieldOne: UIView!
    @IBOutlet weak var mFieldFour: UILabel!
    @IBOutlet weak var mFieldThree: UILabel!
    @IBOutlet weak var mFieldTwo: UILabel!
    var mCalculateOperation : CalculateOperation!
    
    @IBOutlet weak var mContentFieldFive: UIView!
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var mWomanRadioButton: DLRadioButton!
    @IBOutlet weak var mMenRadioButton: DLRadioButton!
    
    @IBOutlet weak var mEditFieldFive: UITextField!
    @IBOutlet weak var mFieldFiveLabel: UILabel!
    @IBOutlet weak var calcularButtom: UIButton!
    
    @IBOutlet weak var mContentRadioFour: UIView!
    @IBOutlet weak var mContentRadioThree: UIView!
    @IBOutlet weak var mContentRadioTwo: UIView!
    var mListLinear : [itemViews] = []
    
    var mMan: Bool = false
    var mWoman : Bool = false
    
    var mYesAleteo: Bool = false
    var mNoAleteo : Bool = false
    
    var mYesEsterno: Bool = false
    var mNoEsterno : Bool = false
    
    var mYesMusc: Bool = false
    var mNoMusc : Bool = false
    
    @IBOutlet weak var mYesAleteoBtn: DLRadioButton!
    @IBOutlet weak var mNoAleteoBtn: DLRadioButton!

    @IBOutlet weak var mYesEsternoBtn: DLRadioButton!
    @IBOutlet weak var mNoEsternoBtn: DLRadioButton!

    @IBOutlet weak var mYesMuscBtn: DLRadioButton!
    @IBOutlet weak var mNoMuscBtn: DLRadioButton!
    
    @IBOutlet weak var mContentRadioGroup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false;
        mCalculateOperation = CalculateOperation()
        formSelected = mFormulas.mFunctionCalculate
        calcularButtom.addTarget(self, action: #selector(calculateOperation(_:)), for: .touchUpInside)
        setColors()
        hideAndShow()
        /*if(mFormulas.mDescription.isEmpty || mFormulas.mDescription == ""){
         }else{
         mDetailFormula.text = mFormulas.mDescription
         }*/
        act_form_name.text = mFormulas.mNombreFormula
        if (mFormulas.mFunctionCalculate.contains("CalculateWeightManOutSDRA") || mFormulas.mFunctionCalculate.contains("CalculateWeightManWithSDRA")) {
            ViewUtils.setVisibility(view: mContentRadioGroup, visibility: .VISIBLE)
            ViewUtils.setVisibility(view: mContentRadioTwo, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioThree, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioFour, visibility: .INVISIBLE)
            mHeightConstraint.constant = 55
            mHeightConstraintTwo.constant = 0
            mHeightConstraintThree.constant = 0
            mHeightConstraintFour.constant = 0
            mContentRadioGroup.layoutIfNeeded()
            mContentRadioTwo.layoutIfNeeded()
            mContentRadioThree.layoutIfNeeded()
            mContentRadioFour.layoutIfNeeded()
        }else if(mFormulas.mFunctionCalculate == "CalculateWOBSCORE"){
            ViewUtils.setVisibility(view: mContentRadioGroup, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioTwo, visibility: .VISIBLE)
            ViewUtils.setVisibility(view: mContentRadioThree, visibility: .VISIBLE)
            ViewUtils.setVisibility(view: mContentRadioFour, visibility: .VISIBLE)
            mHeightConstraint.constant = 0
            mHeightConstraintTwo.constant = 55
            mHeightConstraintThree.constant = 55
            mHeightConstraintFour.constant = 55
            mContentRadioGroup.layoutIfNeeded()
            mContentRadioTwo.layoutIfNeeded()
            mContentRadioThree.layoutIfNeeded()
            mContentRadioFour.layoutIfNeeded()
        }else{
            ViewUtils.setVisibility(view: mContentRadioGroup, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioTwo, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioThree, visibility: .INVISIBLE)
            ViewUtils.setVisibility(view: mContentRadioFour, visibility: .INVISIBLE)
            mHeightConstraint.constant = 0
            mHeightConstraintTwo.constant = 0
            mHeightConstraintThree.constant = 0
            mHeightConstraintFour.constant = 0

            mContentRadioGroup.layoutIfNeeded()
            mContentRadioTwo.layoutIfNeeded()
            mContentRadioThree.layoutIfNeeded()
            mContentRadioFour.layoutIfNeeded()

        }
    }
    
    @objc func calculateOperation(_ sender: UIButton){
        selectFormula()
    }
    
    func hideAndShow(){
        setList()
        let size = mListLinear.count - mFormulas.mParams.count
        
        if(size > 0){
            for index in 0...size-1 {
                ViewUtils.setVisibility(view: mListLinear[index].mContainer, visibility: .INVISIBLE)
            }
        }
        
        populateInfo()
    }
    
    func populateInfo(){
        revertList()
        for index in 0...mFormulas.mParams.count-1 {
            mListLinear[index].mLabel.text = mFormulas.mParams[index]
            mListLinear[index].mTextField.placeholder = mFormulas.mParams[index]
        }
    }
    
    func revertList(){
        mListLinear.removeAll()
        mListLinear.append(itemViews(mContainer: mContentFieldOne, mLabel: mFieldOne, mTextField: mEditFieldOne))
        mListLinear.append(itemViews(mContainer: mContentFieldTwo, mLabel: mFieldTwo, mTextField: mEditFieldTwo))
        mListLinear.append(itemViews(mContainer: mContentFieldThree, mLabel: mFieldThree, mTextField: mEditFieldThree))
        mListLinear.append(itemViews(mContainer: mContentFieldFour, mLabel: mFieldFour, mTextField: mEditFieldFour))
        mListLinear.append(itemViews(mContainer: mContentFieldFive, mLabel: mFieldFiveLabel, mTextField: mEditFieldFive))
    }
    
    func setList(){
        mListLinear.removeAll()
        mListLinear.append(itemViews(mContainer: mContentFieldFive, mLabel: mFieldFiveLabel, mTextField: mEditFieldFive))
        mListLinear.append(itemViews(mContainer: mContentFieldFour, mLabel: mFieldFour, mTextField: mEditFieldFour))
        mListLinear.append(itemViews(mContainer: mContentFieldThree, mLabel: mFieldThree, mTextField: mEditFieldThree))
        mListLinear.append(itemViews(mContainer: mContentFieldTwo, mLabel: mFieldTwo, mTextField: mEditFieldTwo))
        mListLinear.append(itemViews(mContainer: mContentFieldOne, mLabel: mFieldOne, mTextField: mEditFieldOne))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(netHex: Colors.color_white)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = NSLocalizedString("operation_calculator", comment: "")
        navigationController?.navigationBar.backgroundColor = UIColor.init(red: 9, green: 26, blue: 160)
        navigationController?.navigationBar.barTintColor =   UIColor.init(red: 9, green: 26, blue: 160)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func launchDialog() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "OperationResultViewController") as! OperationResultViewController
        v1.mResult = mResult.toString()
        v1.mFormula = formSelected
        MIBlurPopup.show(v1, on: self)
    }
    
    func launchDialogError(mMessage : String) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        v1.mLegend = mMessage
        MIBlurPopup.show(v1, on: self)
    }
    
    func selectFormula() {
        switch (mFormulas.mFunctionCalculate) {
        case "CalculateWeightManOutSDRA":
            if (!mEditFieldOne.text!.isEmpty) {
                if (mMan || mWoman) {
                    if (mMan) {
                        mResult = mCalculateOperation.CalculateWeightManWithSDRA(size: (replaceDat(mRes:mEditFieldOne.text!)))
                    } else {
                        mResult = mCalculateOperation.CalculateWeightWomanWithSDRA(size: replaceDat(mRes:(mEditFieldOne.text!)))
                    }
                    launchDialog();
                } else {
                    launchDialogError(mMessage: NSLocalizedString("select_gender", comment: ""))
                }
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateWeightManWithSDRA":
            if (!mEditFieldOne.text!.isEmpty) {
                if (mMan || mWoman) {
                    if (mMan) {
                        mResult = mCalculateOperation.CalculateWeightManWithSDRA(size: replaceDat(mRes: mEditFieldOne.text!))
                    } else {
                        mResult = mCalculateOperation.CalculateWeightWomanWithSDRA(size: replaceDat(mRes:mEditFieldOne.text!))
                    }
                    launchDialog()
                } else {
                    launchDialogError(mMessage: NSLocalizedString("select_gender", comment: ""))
                }
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePEEPInitial":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePEEPInitial(weight: replaceDat(mRes:mEditFieldOne.text!), meter: replaceDat(mRes: mEditFieldTwo.text!))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateAcidosis":
            if (!mEditFieldOne.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateAcidosis(HCO: replaceDat(mRes:(mEditFieldOne.text!)))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateAlcalosis":
            if (!mEditFieldOne.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateAlcalosis(HCO: replaceDat(mRes:(mEditFieldOne.text!)))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateFR":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.AjusteFR(fr: replaceDat(mRes:(mEditFieldOne.text!)), pCO2A: replaceDat(mRes:mEditFieldTwo.text!), pCO2D: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateVR":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateVR(cO2:replaceDat(mRes: mEditFieldOne.text!), vt: replaceDat(mRes:(mEditFieldTwo.text!)), eCO2: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateDistencionPulmonar":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateDistencionPulmonar(mPresionMeseta: replaceDat(mRes:mEditFieldOne.text!), mPEEP: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateRespiracionesRapidasSuperficiales":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateRespiracionesRapidasSuperficiales(mFR: replaceDat(mRes:mEditFieldOne.text!), mVC: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePoderMecanicoVentilatorio":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty && !mEditFieldFour.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePoderMecanicoVentilatorio(DP: replaceDat(mRes:mEditFieldOne.text!), Pmax: replaceDat(mRes:mEditFieldTwo.text!), FR: replaceDat(mRes:mEditFieldThree.text!), Vt: replaceDat(mRes:mEditFieldFour.text!))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionParcialO2A":
            if (!mEditFieldOne.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePresionParcialO2A(mEdad: replaceDat(mRes:mEditFieldOne.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionParcialO2B":
            if (!mEditFieldOne.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePresionParcialO2A(mEdad: replaceDat(mRes:mEditFieldOne.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionInspiradaO2":
            if (!(mEditFieldOne.text!.isEmpty) && (!mEditFieldTwo.text!.isEmpty)) {
                mResult = mCalculateOperation.CalculatePresionInspiradaO2(mFiO2: replaceDat(mRes:mEditFieldOne.text!), mPB: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionAlveolar":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePresionAlveolar(PIO2: replaceDat(mRes:mEditFieldOne.text!), PaCO2: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionAlveolarO2B":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePresionAlveolarO2B(mPIO2: replaceDat(mRes:mEditFieldOne.text!), mPaCO2: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateDiferenciaAlveoloArterialO2":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateDiferenciaAlveoloArterialO2(mPAO2: replaceDat(mRes:mEditFieldOne.text!), PaO2: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateRelacionArterioAlveolarO2":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateRelacionArterioAlveolarO2(mPAO2: replaceDat(mRes:mEditFieldOne.text!), PaO2: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateRelacionOxigenaciónO2":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateRelacionOxigenaciónO2(mPAO2: replaceDat(mRes:mEditFieldOne.text!), mFiO2:replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateRespiratorioO2":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateRespiratorioO2(PAaO2: replaceDat(mRes:mEditFieldOne.text!), PaO2:replaceDat(mRes: mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatOoxigenación":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatOoxigenación(mFiO2: replaceDat(mRes:mEditFieldOne.text!),mPaw: replaceDat(mRes:mEditFieldTwo.text!), mPaO2: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateCortocircuitosIntrapulmonares":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateCortocircuitosIntrapulmonares(mCcO2: replaceDat(mRes:mEditFieldOne.text!), mCaO2: replaceDat(mRes:mEditFieldTwo.text!),mCvO2: replaceDat(mRes: mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        //NUEVAS
        case "CalculateDistensibilidadPulmonarEstatica":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateDistensibilidadPulmonarEstatica(mVt: replaceDat(mRes:mEditFieldOne.text!), mPmeseta: replaceDat(mRes:mEditFieldTwo.text!),mPEEP: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateDistensibilidadPulmonarDinamica":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateDistensibilidadPulmonarDinamica(mVt: replaceDat(mRes: mEditFieldOne.text!), mPmax: replaceDat(mRes:mEditFieldTwo.text!), mPEEP: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculatePresionTransviaAerea":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculatePresionTransviaAerea(mPmax: replaceDat(mRes:mEditFieldOne.text!), mPmeseta: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateResistenciaViaAerea":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateResistenciaViaAerea(mPmax: replaceDat(mRes:mEditFieldOne.text!), mPmeseta: replaceDat(mRes:mEditFieldTwo.text!),mFlujo: replaceDat(mRes:mEditFieldThree.text!));
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
            
        case "CalculateSaO2FiO2":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateSaO2FiO2(mSaO2: replaceDat(mRes:mEditFieldOne.text!), mFiO2: replaceDat(mRes: mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateIndiceROX":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateIndiceROX(mSpO2: replaceDat(mRes:mEditFieldOne.text!), mFiO2: replaceDat(mRes:mEditFieldTwo.text!), mFR:replaceDat(mRes: mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateVentilatoryRatio":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateVentilatoryRatio(mVolMin: replaceDat(mRes:mEditFieldOne.text!), mPaCO2: replaceDat(mRes:mEditFieldTwo.text!), mPP: replaceDat(mRes:mEditFieldThree.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateEspacioMuerto":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty) {
                mResult = mCalculateOperation.CalculateVentilatoryRatio(mPaCO2: replaceDat(mRes:mEditFieldOne.text!), mPECO2: replaceDat(mRes:mEditFieldTwo.text!))
                launchDialog();
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break;
        case "CalculateROX":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty && !mEditFieldFour.text!.isEmpty) {
                mResult = mCalculateOperation.calcultaeROXHR(mSAO2: replaceDat(mRes: mEditFieldOne.text!), mFiO2: replaceDat(mRes: mEditFieldTwo.text!), mFR: replaceDat(mRes: mEditFieldThree.text!), mFC: replaceDat(mRes: mEditFieldFour.text!))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break
        
        case "CalculateHARCORE":
            if (!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty && !mEditFieldFour.text!.isEmpty && !mEditFieldFive.text!.isEmpty) {
                mResult = mCalculateOperation.calcultareHARCORE(mFC: replaceDat(mRes: mEditFieldOne.text!), mEDCDG: replaceDat(mRes: mEditFieldTwo.text!), mPH: replaceDat(mRes: mEditFieldThree.text!), mFR: replaceDat(mRes: mEditFieldFour.text!), mPAO2FIO2: replaceDat(mRes: mEditFieldFive.text!))
                launchDialog()
            } else {
                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
            }
            break
            
        case "CalculateWOBSCORE":
            if (!mEditFieldOne.text!.isEmpty) {
                                if (mYesAleteo || mYesEsterno || mYesMusc || mNoAleteo || mNoEsterno || mNoMusc) {
                                    if (mYesAleteo && mYesEsterno && mYesMusc) {
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 1)
                                        launchDialog()
                                    } else if(mYesAleteo && mNoEsterno && mNoMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 2)
                                        launchDialog()
                                    } else if (mNoAleteo && mYesEsterno && mNoMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 3)
                                        launchDialog()
                                    } else if (mNoAleteo && mNoEsterno && mYesMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 4)
                                        launchDialog()
                                    }else if (mYesAleteo && mYesEsterno && mNoMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 5)
                                        launchDialog()
                                    }else if (mYesAleteo && mNoEsterno && mYesMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 6)
                                        launchDialog()
                                    }else if (mNoAleteo && mYesEsterno && mYesMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 7)
                                        launchDialog()
                                    }else if (mNoAleteo && mNoEsterno && mNoMusc){
                                        mResult = mCalculateOperation.calculateWEBSCORE(mFR: replaceDat(mRes: mEditFieldOne.text!), escenario: 0)
                                        launchDialog()
                                    }
                                } else {
                                    //startActivity(new Intent(this, AlertDialogNoCheckActivity.class));
                                    launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
                                }
                            } else {
                                launchDialogError(mMessage: NSLocalizedString("enter_required_data", comment: ""))
                            }
            break
        default:
            break
        }
        
        
    }
    @IBAction func mYesAleteoRB(_ sender: Any) {
        print("Men Checked")
        mYesAleteo = true
        mNoAleteo = false
        
        mYesAleteoBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoAleteoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
    }
    
    @IBAction func mNoAleteoRB(_ sender: Any) {
        mYesAleteo = false
        mNoAleteo = true
        
        mNoAleteoBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mYesAleteoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    @IBAction func mYesEsternoRB(_ sender: Any) {
        mYesEsterno = true
        mNoEsterno = false
        
        mYesEsternoBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoEsternoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    @IBAction func mNoEsternoRB(_ sender: Any) {
        mYesEsterno = false
        mNoEsterno = true
        
        mNoEsternoBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mYesEsternoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    @IBAction func mYesMuscRB(_ sender: Any) {
        mYesMusc = true
        mNoMusc = false
        
        mYesMuscBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoMuscBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    @IBAction func mNoMuscRB(_ sender: Any) {
        mYesMusc = false
        mNoMusc = true
        
        mNoMuscBtn.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mYesMuscBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    @IBAction func mMenRadioButton(_ sender: Any) {
        print("Men Checked")
        mMan = true
        mWoman = false
        mMenRadioButton.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mWomanRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    @IBAction func mWomanRadioButton(_ sender: Any) {
        print("Woman Checked")
        mWoman = true
        mMan = false
        mWomanRadioButton.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mMenRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
    
    func setColors(){
        mWomanRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mMenRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        
        mYesAleteoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoAleteoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mYesEsternoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoEsternoBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mYesMuscBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
        
        mNoMuscBtn.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_background_app)), for: .normal)
    }
   
    func replaceDat(mRes : String)->Double{
           let aString = mRes
           let newString = aString.replacingOccurrences(of: ",", with: ".")
           return newString.toDouble()!
    }
}

extension String {
    func toDouble() -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale(localeIdentifier: "es_MX") as Locale
        return numberFormatter.number(from: self)?.doubleValue
    }
}

extension UIImage {
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
    
    func toString3() -> String {
        return String(format: "%.3f",self)
    }
    
    func toString14() -> String {
        return String(format: "%.14f",self)
    }
}


extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = self.layer.cornerRadius
        
        self.layer.addSublayer(gradientLayer)
    }
}


extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        guard let index = tabBarController?.selectedIndex else { return .default }
        switch index {
        case 0, 1, 2: return .lightContent // set lightContent for tabs 0-2
        default: return .default // set dark for tab 3
        }
    }
}
