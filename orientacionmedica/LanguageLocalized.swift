//
//  LanguageLocalized.swift
//  orientacionmedica
//
//  Created by Carlos Salazar Vazquez on 19/01/23.
//  Copyright © 2023 avento. All rights reserved.
//

import UIKit

class LanguageLocalized {

    func isLanguageUS()->Bool{
        if let countryCode = NSLocale.current.languageCode {
            switch countryCode{
            case "US","us":return true
                
            case "EN", "en":return true

            case "EN-US","en-us":return true
                
            case "ES","es":return false
                
            default:return false
            }
        }else{
            return false
        }
    }
    
    /***
     Spanish Language D*/
    let menu_home = "Inicio"
    let menu_subtitle = "¡Tú mides, tú calculas, tú programas!"
    let menu_description = "Programación protectora de ventilación mecánica para todas las áreas, metas de monitoreo ventilatorio, taller de gases arteriales y más."
    let menu_download_cards = "Descarga las Tarjetas AVENTHO  y más en la siguiente liga...\nhttps://siemprevirtual.com/tarjetasaventho/"
    let menu_formulas = "Fórmulas"
    let menu_recomendaciones = "Recomendaciones"
    let menu_contactanos = "Contáctanos"
    let menu_conceptos = "Conceptos"
    let title_body = "Ingresa los datos obligatorios *"
    let email_valid = "Ingresa una dirección de correo válida"
    let comment_success = "Tu comentario se ha enviado exitosamente"
    let title_body2 = "Selecciona un sexo para continuar"
    let title_lenguage = "Idioma"
    let change_lenguage = "Selecciona el idioma:"
    let change_lenguage_esp = "ESPAÑOL"
    let change_lenguage_eng = "INGLÉS"
    let change_lenguage_save = "Guardar"

    let fragment_two_tittle_1 = "Fórmulas"
    let fragment_two_tittle_2 = "Mecánica Ventilatoria"
    let fragment_two_tittle_3 = "Taller Gasométrico"

    //Nombres Fórumlas
    let name_form_indice_rox = "Índice ROX"
    let description_form_rox = "Índice ROX"
    let name_form_indice_ecuacion_bohr = "Ecuación de Bohr para obtener % de Espacio Muerto"
    let description_form_ecuacion_bohr = "Ecuación de Bohr para obtener % de Espacio Muerto"
    let name_form_peso_ideal = "Peso ideal"
    let description_form_peso_ideal = "Peso ideal (Pacientes sin SDRA)"
    let name_form_peso_predicho = "Peso predicho para SDRA"
    let description_form_peso_prdicho = "Peso predicho para SDRA (ARDSnet)"
    let name_form_peep_inicial = "PEEP inicial"
    let description_form_peep_inicial = "IMC"
    let name_form_correccion_co2 = "Corrección de CO2"
    let description_form_acidiosis = "CO2 para Alcalosis Metabólica"
    let description_form_alcalosis = "CO2 para Alcalosis Metabólica"
    let description_form_ajust_fr = "Ajuste de la FR para el CO2 deseado"
    let name_form_rox_hr = "ROX-HR"
    let description_form_rox_hr = "Cánulas de Alto Flujo de O2"
    let name_form_hacor_score = "HACOR SCORE"
    let description_form_hacor_score = "Para predecir éxito con el uso de ventilación No Invasiva"
    let name_form_wob_score = "WOB Score"
    let description_form_wob_score = "Para evaluar necesidad de intubación"

    let name_form_presion_baro = "Presión Barométrica (PB)"
    let name_form_presion_vapor = "Presión de Vapor de Agua [P H2O]"
    let name_form_presion_parcial = "Presión parcial de O2 Arterial (PaO2)"
    let name_form_presion_inspi = "Presión Inspirada de O2 (PIO2)"
    let name_form_presion_alveolar = "Presión Alveolar de O2 (PAO2)"
    let name_form_dif_alveolar = "Diferencia Alveolo-arterial de O2 [P(A-a)O2]"
    let name_form_corto_intra = "Cortocircuitos Intrapulmonares (Qs/Qt)"
    let name_form_poder_meca = "Poder mecánico"

    //Variables
    let taller_gas_var_presion_baro = "Presión Barométrica (ej. Nivel del Mar: 760 mm Hg)"
    let taller_gas_var_fio2 = "FiO2 (%)"
    let taller_gas_var_pao2 = "PaO2 (mm Hg)"
    let taller_gas_var_pvo2 = "PvO2 (mm Hg)"
    let taller_gas_var_paco2 = "PaCO2 (mm Hg)"
    let taller_gas_var_pvco2 = "PvCO2 (mm Hg)"
    let taller_gas_var_sao2 = "SaO2 (en decimales, ej. 95% es 0.95)"
    let taller_gas_var_svo2 = "SvO2 (en decimales, ej. 70% es 0.7)"
    let taller_gas_var_hemo = "Hemoglobina (mg/dl)"
    let btn_calculate = "Calcular"
    let btn_aceptar = "Aceptar"
    let btn_cerrar = "Cerrar"
    let title_results = "Resultados"
    let title_send = "Enviar"

    let form_full_name = "  Nombre completo: *"
    let form_email = "  Correo electrónico: *"
    let form_comment = "  Comentario: *"
    let form_send_comment = " Envíanos un comentario:"
    let form_follow_me = "Síguenos en nuestras redes sociales"

    let mecanica_vent_var_pres_max = "Presión Máxima (cm H2O)"
    let mecanica_vent_var_pres_mese = "Presión Meseta (cm H2O)"
    let mecanica_vent_var_peep = "PEEP (cm H2O)"
    let mecanica_vent_var_vt = "Vt ml"
    let mecanica_vent_var_flujo = "Flujo (L/min)"
    let mecanica_vent_var_frec_resp = "Frecuencia Respiratoria"
    let mecanica_vent_var_paco2 = "PaCO2 (mm Hg)"
    let mecanica_vent_var_peso_pred = "Peso Predicho"

    //Variables resultado Taller Gasometrico
    let mecanica_vent_ps = "PIO2 Presión Inspirada de O2(mm Hg)"
    let mecanica_vent_pa = "PAO2 Presión Alveolar de O2(mm Hg)"
    let mecanica_vent_da = "DAaO2 Diferencia Alveolo-arterial de O2(mm Hg)"
    let mecanica_vent_ca = "CaO2 Contenido arterial de O2(mL/DL)"
    let mecanica_vent_cv = "CvO2 Contenido venoso de O2(mm Hg)"
    let mecanica_vent_c = "Cortocircuitos"
    let mecanica_vent_ra = "Relación arterio-Alveolar de O2"
    let mecanica_vent_ir = "Índice Respiratorio"
    let mecanica_vent_ik = "Índice de Kirby PaO2/FiO2"
    let mecanica_vent_safi = "SaO2/FiO2"
    let mecanica_vent_dv = "DvaCO2 Diferencia veno-arterial de CO2 (mm Hg)"
    let mecanica_vent_ieo = "IEO2 (%)"

    //Resultados Taller Gasometrico
    let mecanica_vent_resul3 = "%s\nNormal 5 a 10 mm Hg\n>10 mm Hg = Hipoxemia por trastorno de Difusión \n"
    let mecanica_vent_resul4 = "%s\nNormal 15 a 19 ml/dL"
    let mecanica_vent_resul5 = "%s\nNormal 11 a 15 ml/dL"
    let mecanica_vent_resul6 = "%s\nNormal 5 a 10%%\n>10%% = Hipoxemia por incremento en los Cortocircuitos\n"
    let mecanica_vent_resut7 = "%s\nNormal: 0.7 a 0.9\n&lt;0.7 = Hipoxemia por trastorno de Difusión\n"
    let mecanica_vent_resul9 = "%s\nNo SDRA : >300\nSDRA Leve: 300 a 200\nSDRA Moderado: 199 a 100\nSDRA Severo: &lt;100\n"
    let mecanica_vent_resul10 = "%s\nNo SDRA >315\nSDRA &lt;315\nSDRA moderado-severo: &lt;190\n"
    let mecanica_vent_resut11 = "%s\nNormal = &lt;6mmHg\n>6 mm Hg = Hipoperfusión tisular\n"
    let mecanica_vent_resut12 = "\nNormal = 20%% a 30%%\n" + "&lt;20%% = Isquemia tisular\n" +  "30%% = Hiperflujo de oxígeno\n"

    //Resultados Mecánica Ventilaria
    let taller_gaso_result_driving = "%s\nSi > 15 considere:\nBajar Vt, Modificar PEEP, Posición Prona\n"
    let taller_gaso_result_presion = "%s\nSi es >4 verifique si hay obstrucción en la vía aérea\n(broncoespásmo, secreciones, tubo doblado\n"
    let taller_gaso_result_poder = "%s\nSIN ARDS = >12 se asocia a mayor mortalidad\nARDS >22.4 se asocia a mayor mortalidad\n"
    let taller_gaso_result_ratio = "%s\n>1.5 se asocia a mayor mortalidad"

    //Variables resultado Mecánica Ventilaria
    let mecanica_vent_dp = "Driving Pressure"
    let mecanica_vent_rp = "Presión Transaérea"
    let mecanica_vent_de = "Distensibilidad Estática (Cst)"
    let mecanica_vent_dd = "Distensibilidad Dinámica (Cdyn)"
    let mecanica_vent_rv = "Resistencia de la Vía Aérea (Raw)"
    let mecanica_vent_ee = "Elastansa Estática (Est)"
    let mecanica_vent_ed = "Elastansa Dinámica (Edyn)"
    let mecanica_vent_vr = "Ventilatory Ratio"

    let description_form_empty = ""

    let conceptos = "Conceptos"
    let buscar_conceptos = "Buscar conceptos"
    let description_form1 = "Es la cantidad de aire que entra en los pulmones que entrará con cada inspiración programada.\nEl Vt a programar se obtiene en 3 pasos:\n1)    Medir la talla de altura del paciente con cinta métrica.\n2)    Calcular el peso ideal (pacientes sin SDRA) o peso predicho para pacientes con SDRA.\nMultiplicar por los ml deseados (6 a 10 ml x kg de peso ideal para pacientes sin SDRA ó 4 a 8 ml x kg de peso predicho para pacientes con SDRA).  "
    let description_form2 = "El peso ideal es una cifra apropiada e individualizada para el cálculo adecuado del volumen corriente (Vt), propuesta por la Organización Mundial de la Salud (OMS) y adaptada a la ventilación mecánica, basada en el IMC normal para cada talla con la finalidad de evitar el daño inducido por Vt excesivo (volutrauma).\nPuede usarse en adultos y niños de todas las tallas, con controversia de su uso para neonatos a término y pretérmino."
    let description_form3 = "El peso predicho es una cifra propuesta para el cálculo del volumen corriente (Vt), propuesta por grupo ARDSnet, para usarse en pacientes que presntan Síndrome de Distrés Respiratorio Agudo (SDRA) basada en el IMC normal para cada talla con la finalidad de evitar el daño inducido por Vt excesivo (volutrauma). Lo que ha demostrado mejoría en la supervivencia de estos pacientes.\nPuede usarse en adultos que midan más de 152.4 cm."
    let description_form4 = "La presión positiva al final de la espiración (PEEP) puede ser inicialmente programada de acuerdo al índice de masa corporal del paciente (IMC)."
    let description_form5 = "La presión sanguínea de dióxido de carbono debe estar ajustada de acuerdo a varios factores, debido a que causa alteraciones del tono vasomotor a nivel pulmonar, cerebral y otros órganos importantes, en paciente pulmonarmente sano la meta será una paCO2 normal para la altura a la que esté habituado el paciente sobre el nivel del mar.\nEl CO2 se modificará con la Frecuencia Respiratoria programada (FR), aumentar la FR generará menor CO2 arterial, disminuir la FR aumentará el CO2 arterial (no aplica para pacientes con atrapamiento aéreo)\nEn pacientes que tengan trastornos metabólicos del estado ácido-base deberá ser ajustado basado en el CO2 (CO2e) esperado ya sea para acidosis metabólica o alcalosis metabólica, se requiere una gasometría arterial para determinar la paCO2 actual (presión arterial parcial de dióxido de carbono).\nPara los pacientes que tengan retención crónica de dióxido de carbono (ej. enfermedad pulmonar obstructiva crónica, síndrome de apnea obstructiva del sueño) deberá ser ajustada de acuerdo a la paCO2 que les permita el mayor equilibrio ácido-base (pH cercano a 7.35)."
    let description_form6 = "Presión ejercida por la atmósfera a una altura determinada.\n760 mm Hg (a nivel del mar)."
    let description_form7 = "Presión ejercida por el vapor de agua que ingresa a la vía respiratoria. 47 mm HG."
    let description_form8 = "Presión de oxígeno arterial a disposición de los tejidos. Normal 85 a 100 mm Hg a nivel del mar. Hasta 60 o 55 mm Hg a mayores alturas."
    let description_form9 = "Presión de oxígeno que ingresa a la vía respiratoria de conducción."
    let description_form10 = "Presión de oxígeno que se encuentra dentro de los alveolos."
    let description_form11 = "Diferencia de presión de oxígeno alveolar al arterial, depende de la capacidad de difusión de la membrana alveolo-capilar."
    let description_form12 = "Porcentaje de sangre arterial que pasa por la circulación pulmonar sin oxigenarse. Normal menor a 10%."
    let description_form13 = "Energía total suministrada por el ventilador al tejido pulmonar en una unidad de tiempo.\n\n Menor a 12 J/min se asocia a menor lesión inducida por el ventilador."

    let result_msg_CalculateWeightManOutSDRA = "Multiplique el resultado del peso ideal por 6 a 10 ml para obtener el Vt,\nEjemplo: 8 ml x 70 kg de peso ideal = Vt: 560 ml"
    let result_msg_CalculateWeightManWithSDRA = "Multiplique el resultado del peso ideal por 4 a 8 ml para obtener el Vt,\nEjemplo: 6 ml x 70 kg de peso ideal = Vt: 420 ml"
    let result_msg_CalculateWeightManWithSDRA1 = "El resultado se muestra en %\n\nNormal menor a 10%"
    let result_msg_CalculateEspacioMuerto = "El resultado se expresa en %\n\nNormal es menor a 30%."
    let result_msg_CalculateFR = "Coloque esta FR para obtener el CO2 deseado, verifique una relación I:E fisiológica (ej. 1:2)."
    let result_msg_CalculatePresionParcialO2B = "Normal 85 a 100 mm Hg a nivel del mar. Hasta 60 o 55 mm Hg a mayores alturas."
    let result_msg_CalculateDiferenciaAlveoloArterialO2 = "Normal 10 a 15 mm HG"
    let result_msg_CalculateRelacionOxigenaciónO2 = "Normal >300"
    let result_msg_CalculateCortocircuitosIntrapulmonares = "El resultado se muestra en %"
    let result_msg_PPEP_initial_1 = "Considere PEEP inicial: 5 a 8 cm H2O"
    let result_msg_PEEP_initial_2 = "Considere PEEP inicial: 10 cm H2O"
    let result_msg_PEEP_initial_3 = "Considere PEEP inicial: 10 cm H2O"

    let result_msg_1 = "No extubar"
    let result_msg_2 = "Valorar extubacion"
    let result_msg_3 = "Más éxito en extubación"

    let result_msg_pmv_1 = "Sugiere protección pulmonar"
    let result_msg_pmv_2 = "Valorar ajustar parámetros para lograr protección pulmonar (ejem. bajar Vt, bajar Pmeseta, bajar FR)"

    let result_msg_dp_1 = "Se encuentra en meta de protección pulmonar"
    let result_msg_dp_2 = "Fuera de rango de protección pulmonar"
    let title_activity_main2 = "Inicio"
    let navigation_drawer_open = "Open navigation drawer"
    let navigation_drawer_close = "Close navigation drawer"

    let result_msg_safi_1 = "Indicativo de Distres Respiratorio Moderado a Severo equivalente a menor a 150 de PaO2/FiO2"
    let result_msg_safi_2 = "Correlaciona con PaO2/FiO2 mayor a 150"

    let result_msg_indice_rox_1 = "Bajo riesgo de intubación o reintubación."
    let result_msg_indice_rox_2 = "Alto riesgo de intubación o reintubacion."
    let result_msg_indice_rox_3 = "Vigilancia estrecha."

    let result_msg_ratio_1 = "Incrementó en el espacio muerto."
    let result_msg_ratio_2 = "Riesgo incrementado de mortalidad."

    //Fórmulas nuevas 04-01-2023

    let result_msg_rox_1 = "Predice fallo a la terapia con alto flujo de O2."

    let result_msg_hacor_1 = "Necesidad de intubación."
    let result_msg_hacor_2 = "No hay necesidad de intubación"

    let result_msg_wob_1 = "Posible necesidad de intubación."
    let result_msg_wob_2 = "No hay necesidad de intubación"

    ///****
    ///English

    let ENmenu_home = "Home"
    let ENmenu_subtitle = "You measure, calculate, and adjust the vent!"
    let ENmenu_description = "Protective mechanical ventilation settings for all areas, ventilation monitoring goals, ABG analysis, and more. "
    let ENmenu_download_cards = " "
    let ENmenu_formulas = "Formulas"
    let ENmenu_recomendaciones = "Recommendations"
    let ENmenu_contactanos = "Contact"
    let ENmenu_conceptos = "Concepts and definitions"
    let ENtitle_body = "Enter the required information *"
    let ENemail_valid = "Enter a valid email address"
    let ENcomment_success = "Your comment has been sent successfully"
    let ENtitle_body2 = "Select a gender to continue"
    let ENtitle_lenguage = "Language"
    let ENchange_lenguage = "Select the language:"
    let ENchange_lenguage_esp = "SPANISH"
    let ENchange_lenguage_eng = "ENGLISH"
    let ENchange_lenguage_save = "Save"

    let ENfragment_two_tittle_1 = "Formulas"
    let ENfragment_two_tittle_2 = "Mechanical Power"
    let ENfragment_two_tittle_3 = "ABG analysis"

    //Nombres Fórumlas
    let ENname_form_indice_rox = "ROX index"
    let ENdescription_form_rox = "ROX index"
    let ENname_form_indice_ecuacion_bohr = "Bohr equation for % of death space"
    let ENdescription_form_ecuacion_bohr = "Bohr equation for % of death space"
    let ENname_form_peso_ideal = ""
    let ENdescription_form_peso_ideal = ""
    let ENname_form_peso_predicho = "Predicted Body Weight"
    let ENdescription_form_peso_prdicho = "Predicted Body Weight"
    let ENname_form_peep_inicial = "Initial PEEP"
    let ENdescription_form_peep_inicial = "BMI"
    let ENname_form_correccion_co2 = "CO2 correction"
    let ENdescription_form_acidiosis = "expected CO2 in metabolic alcalosis"
    let ENdescription_form_alcalosis = "expected CO2 in metabolic alcalosis"
    let ENdescription_form_ajust_fr = "RR adjustment for the expected CO2"
    let ENname_form_rox_hr = "ROX-HR"
    let ENdescription_form_rox_hr = "High flow-nasal cannula "
    let ENname_form_hacor_score = "HACOR score"
    let ENdescription_form_hacor_score = "Prediction for successful use of non-invasive mechanical ventilation"
    let ENname_form_wob_score = "WOB score"
    let ENdescription_form_wob_score = "Use of sternocleidomastoideus"

    let ENname_form_presion_baro = "Atmospheric Pressure (Atm)"
    let ENname_form_presion_vapor = "Water Vapor Pressure [P H2O]"
    let ENname_form_presion_parcial = "Arterial O2 partial pressure (PaO2)"
    let ENname_form_presion_inspi = "Inspired O2 Pressure (IOP2)"
    let ENname_form_presion_alveolar = "Alveolar O2 Pressure (PAO2)"
    let ENname_form_dif_alveolar = "Alveolar-arterial O2 difference [P(A-a)O2]"
    let ENname_form_corto_intra = "Intrapulmonary shunts (Qs/Qt)"
    let ENname_form_poder_meca = "Mechanical power"

    //Variables
    let ENtaller_gas_var_presion_baro = "Atmospheric Pressure (eg Sea Level: 760 mm Hg)"
    let ENtaller_gas_var_fio2 = "FiO2 (%)"
    let ENtaller_gas_var_pao2 = "PaO2 (mm Hg)"
    let ENtaller_gas_var_pvo2 = "PvO2 (mm Hg)"
    let ENtaller_gas_var_paco2 = "PaCO2 (mm Hg)"
    let ENtaller_gas_var_pvco2 = "PvCO2 (mm Hg)"
    let ENtaller_gas_var_sao2 = "SaO2 (in 0-1 scale, eg 95% is 0.95)"
    let ENtaller_gas_var_svo2 = "SvO2 (in 0-1 scale, eg 70% is 0.7)"
    let ENtaller_gas_var_hemo = "Hemoglobin (mg/dl)"
    let ENbtn_calculate = "Calculate"
    let ENbtn_aceptar = "Accept"
    let ENbtn_cerrar = "Close"
    let ENtitle_results = "Results"
    let ENtitle_send = "Send"

    let ENform_full_name = " Full name: *"
    let ENform_email = " Email: *"
    let ENform_comment = " Comment: *"
    let ENform_send_comment = " Send us a comment:"
    let ENform_follow_me = "Follow us on our social media"

    let ENmecanica_vent_var_pres_max = "Airway Pressure (cm H2O)"
    let ENmecanica_vent_var_pres_mese = "Plateau Pressure (cm H2O)"
    let ENmecanica_vent_var_peep = "PEEP (cm H2O)"
    let ENmecanica_vent_var_vt = "Vt ml"
    let ENmecanica_vent_var_flujo = "Flow (L/min)"
    let ENmecanica_vent_var_frec_resp = "Respiratory rate"
    let ENmecanica_vent_var_paco2 = "PaCO2 (mmHg)"
    let ENmecanica_vent_var_peso_pred = "Predicted Body Weight"

    //Variables resultado Taller Gasometrico
    let ENmecanica_vent_ps = "PIO2 Partial Pressure of Inspired Oxygen  (mmHg)"
    let ENmecanica_vent_pa = "-PAO2 Partial Pressure of Alveolar Oxygen  (mmHg)"
    let ENmecanica_vent_da = "-DAaO2 Alveolar-arterial Oxygen difference (mmHg)"
    let ENmecanica_vent_ca = "-CaO2 Arterial Oxygen content (mL/DL)"
    let ENmecanica_vent_cv = "-CvO2 Venous Oxygen content (mmHg)"
    let ENmecanica_vent_c = "Shunts"
    let ENmecanica_vent_ra = "Arterial-alveolar Oxygen ratio"
    let ENmecanica_vent_ir = "Respiratory Index"
    let ENmecanica_vent_ik = "Kirby PaO2/FiO2 Index"
    let ENmecanica_vent_safi = "SaO2/FiO2"
    let ENmecanica_vent_dv = "DvaCO2 Veno-arterial CO2 difference (mmHg)"
    let ENmecanica_vent_ieo = "IEO2 (%)"

    //Resultados Taller Gasometrico
    let ENmecanica_vent_resul3 = "%s\nNormal 5 to 10 mm Hg >10 mm Hg = Hypoxemia due to a diffusion disorder\n"
    let ENmecanica_vent_resul4 = "%s\nNormal 15 to 19 ml/dL"
    let ENmecanica_vent_resul5 = "%s\nNormal 11 to 15 ml/dL"
    let ENmecanica_vent_resul6 = "%s\nNormal 5 to 10%%\n> 10%% = Hypoxemia due to an increase in short-cuts\n"
    let ENmecanica_vent_resut7 = "%s\nNormal 0.7 to 0.9\n&lt;0.7 = Hypoxemia due to a diffusion disorder\n"
    let ENmecanica_vent_resul9 = "%s\nNo ARDS: >300 Mild ARDS: 300 to 200 Moderate ARDS: 199 to 100 Severe ARDS: &lt;100\n"
    let ENmecanica_vent_resul10 = "%s\nNo ARDS >315\nARDS &lt;315 Moderate-severe ARDS: &lt;190\n"
    let ENmecanica_vent_resut11 = "%s\nNormal = &lt;6 mm Hg >6 mm Hg = Tissue hypoperfusion\n"
    let ENmecanica_vent_resut12 = "%s\nNormal = 20% to 30%" + "&lt;20% = Tissue ischemia" +  "30% = Oxygen hyperflow\n"

    //Resultados Taller Mecánica Ventilaria
    let ENtaller_gaso_result_driving = "%s\nIf > 15 consider:\nLower tV, Modify PEEP, Prone Position\n"
    let ENtaller_gaso_result_presion = "%s\nIf it is >4, check if there is obstruction in the airway\n(bronchospasm, secretions, kinked tube\n"
    let ENtaller_gaso_result_poder = "%s\nWITHOUT ARDS = >12 is associated with higher mortality\n ARDS >22.4 is associated with higher mortality\n"
    let ENtaller_gaso_result_ratio = "%s\n>1.5 is associated with higher mortality\n"

    //Variables resultado Mecánica Ventilaria
    let ENmecanica_vent_dp = "Driving Pressure"
    let ENmecanica_vent_rp = "Resistive Pressure"
    let ENmecanica_vent_de = "Static Compliance (Cst)"
    let ENmecanica_vent_dd = "Dynamic Compliance (Cdyn)"
    let ENmecanica_vent_rv = "Airway Resistance (Raw)"
    let ENmecanica_vent_ee = "Static Elastance (Est)"
    let ENmecanica_vent_ed = "Dynamic Elastance (Edyn)"
    let ENmecanica_vent_vr = "Ventilatory Ratio"

    let ENdescription_form_empty = ""

    let ENconceptos = "Concepts"
    let ENbuscar_conceptos = "Search concepts"

    let ENdescription_form1 = "It is the amount (volume) of air that will enter the lungs during each programmed cycle.\n The tV to be programmed is obtained in 3 steps:\n 1) Measure the height of the patient with a tape measure.\n 2) Calculate the ideal weight (non-ARDS patients) or predicted weight for ARDS patients.\n 3) Multiply by mL desired (6 to 10 mL x kg predicted body weight for non-ARDS patients or 4 to 8 mL x kg predicted body weight for ARDS patients)."
    let ENdescription_form2 = ""
    let ENdescription_form3 = "The predicted body weight is variable proposed for the calculation of Vt, proposed by the ARDSnet group (Brower, 2000), for use in patients with Acute Respiratory Distress Syndrome (ARDS) based on the normal BMI for each height, in order to avoid the damage induced by excessive Vt (volutrauma). This has shown improvement in the survival of these patients.\nIt can be used in adults measuring more than 152.4 cm."
    let ENdescription_form4 = "Positive end-expiratory pressure (PEEP) can be initially programmed according to the patient\'s body mass index (BMI) (Lov-ED Study, 2017)."
    let ENdescription_form5 = "The partial pressure of carbon dioxide (pCO2) must be adjusted according to several factors given it causes alterations in vasomotor tone at the lung, brain, and other relevant organs. The goal in a patient without pulmonary pathology will be a normal paCO2 for the level over the sea at which the patient is used to.\n paCO2 will be modified with the programmed Respiratory Rate (RR), increasing the RR will generate less arterial CO2, decreasing the RR will increase the arterial CO2 (does not apply to patients with air entrapment)\n In patients who have metabolic disorders acid-base status should be adjusted based on expected CO2 (eCO2) for either metabolic acidosis or metabolic alkalosis, arterial blood gases are required to determine current paCO2 (arterial partial pressure of carbon dioxide) .\n For patients with chronic carbon dioxide retention (eg, chronic obstructive pulmonary disease, obstructive sleep apnea syndrome) it should be adjusted according to the paCO2 that allows the best acid-base balance (pH close to 7.35)."
    let ENdescription_form6 = "Pressure exerted by the atmosphere at a given height.\n 760 mm Hg (at sea level)."
    let ENdescription_form7 = "Pressure exerted by water vapor entering the respiratory tract (47mmHg)."
    let ENdescription_form8 = "Arterial oxygen pressure available for the tissues. Normal 85 to 100 mm Hg at sea level. Up to 60-55 mmHg at higher altitudes."
    let ENdescription_form9 = "Oxygen pressure entering the conducting airway."
    let ENdescription_form10 = "Oxygen pressure found within the alveoli."
    let ENdescription_form11 = "Difference in alveolar to arterial oxygen pressure, depends on the diffusion capacity of the alveolar-capillary membrane."
    let ENdescription_form12 = "Percentage of arterial blood that goes through the pulmonary circulation without being oxygenated. Typically, less than 10%."
    let ENdescription_form13 = "Total energy delivered by the ventilator to the lung in a unit of time.\n \n Less than 12 J/min is associated with less injury induced by the ventilator."

    let ENresult_msg_CalculateWeightManOutSDRA = "Multiply the result of the predicted body weight by 6 to 10 ml to obtain the Vt,\n Example: 8 ml x 70 kg of ideal weight = Vt: 560 ml"
    let ENresult_msg_CalculateWeightManWithSDRA = "Multiply the result of the predicted body weight by 4 to 8 ml to obtain the Vt,\nExample: 6 ml x 70 kg of ideal weight = Vt: 420 ml"
    let ENresult_msg_CalculateWeightManWithSDRA1 = "The result is shown in % Normal less than 10%"
    let ENresult_msg_CalculateEspacioMuerto = "The result is expressed in % Normal is less than 30%"
    let ENresult_msg_CalculateFR = "Set the RR to obtain the desired paCO2, verify a physiological I:E ratio (eg 1:2)."
    let ENresult_msg_CalculatePresionParcialO2B = "Normal 85 to 100 mmHg at sea level. Up to 60-55 mm Hg at higher altitudes."
    let ENresult_msg_CalculateDiferenciaAlveoloArterialO2 = "Normal 10 to 15 mmHG"
    let ENresult_msg_CalculateRelacionOxigenaciónO2 = "Normal >300"
    let ENresult_msg_CalculateCortocircuitosIntrapulmonares = "The result is displayed in %"
    let ENresult_msg_PPEP_initial_1 = "Consider initial PEEP: 5 to 8 cm H2O"
    let ENresult_msg_PEEP_initial_2 = "Consider initial PEEP: 8 cm H2O"
    let ENresult_msg_PEEP_initial_3 = "Consider initial PEEP: 10 cm H2O"

    let ENresult_msg_1 = "Do not extubate"
    let ENresult_msg_2 = "Assess extubation"
    let ENresult_msg_3 = "More chance for success in extubation"

    let ENresult_msg_pmv_1 = "Suggests lung protection"
    let ENresult_msg_pmv_2 = "Evaluate adjusting parameters to achieve lung protection (eg lower Vt, lower P plateau, lower RR)"

    let ENresult_msg_dp_1 = "The patient is under lung protection goals"
    let ENresult_msg_dp_2 = "Out of lung protection range"
    let ENtitle_activity_main2 = "Home"
    let ENnavigation_drawer_open = "Open navigation drawer"
    let ENnavigation_drawer_close = "Close navigation drawer"

    let ENresult_msg_safi_1 = "Indicative of Moderate to Severe Respiratory Distress equivalent to a PaO2/FiO2 less than 150 PaO2/FiO2"
    let ENresult_msg_safi_2 = "Correlates with PaO2/FiO2 greater than 150"

    let ENresult_msg_indice_rox_1 = "Low risk of intubation or reintubation."
    let ENresult_msg_indice_rox_2 = "High risk of intubation or reintubation."
    let ENresult_msg_indice_rox_3 = "Close surveillance."

    let ENresult_msg_ratio_1 = "Increased in dead space."
    let ENresult_msg_ratio_2 = "Increased risk of mortality."

    //Fórmulas nuevas 04-01-2023

    let ENresult_msg_rox_1 = "Predicts failure to high-flow oxygen therapy"

    let ENresult_msg_hacor_1 = "Need for intubation."
    let ENresult_msg_hacor_2 = "No need for intubation"

    let ENresult_msg_wob_1 = "Possible need for intubation."
    let ENresult_msg_wob_2 = "No need for intubation"

}

