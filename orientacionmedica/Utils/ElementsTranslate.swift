//
//  ElementsTranslate.swift
//  orientacionmedica
//
//  Created by Carlos Salazar Vazquez on 31/01/23.
//  Copyright © 2023 avento. All rights reserved.
//

import UIKit

class ElementsTranslate {
    var mElement : UIView?
    var mType : TypeElement!
    var mText : String?
    
    init(mElement: UIView? = nil, mType: TypeElement!, mText: String? = nil) {
        self.mElement = mElement
        self.mType = mType
        self.mText = mText
    }
}


enum TypeElement {
    case boton
    case etiqueta
}
