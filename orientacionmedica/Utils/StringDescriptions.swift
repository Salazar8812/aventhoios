//
//  StringDescriptions.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/26/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class StringDescriptions: NSObject {
    static let description_form1 = NSLocalizedString("description_form1", comment: "")
    
    static let description_form_empty = ""
    
    static let description_form2 = NSLocalizedString("description_form2", comment: "")
    static let description_form3 = NSLocalizedString("description_form3", comment: "")
    static let description_form4 = NSLocalizedString("description_form4", comment: "")
    static let description_form5 = NSLocalizedString("description_form5", comment: "")
    static let description_form6 = NSLocalizedString("description_form6", comment: "")
    static let description_form7 = NSLocalizedString("description_form7", comment: "")
    static let description_form8 = NSLocalizedString("description_form8", comment: "")
    static let description_form9 = NSLocalizedString("description_form9", comment: "")
    static let description_form10 = NSLocalizedString("description_form10", comment: "")
    static let description_form11 = NSLocalizedString("description_form11", comment: "")
    static let description_form12 = NSLocalizedString("description_form12", comment: "")
    static let description_form13 = NSLocalizedString("description_form13", comment: "")
    
    static let result_msg_CalculateWeightManOutSDRA = NSLocalizedString("result_msg_CalculateWeightManOutSDRA", comment: "")
    static let result_msg_CalculateWeightManWithSDRA = NSLocalizedString("result_msg_CalculateWeightManWithSDRA", comment: "")
    static let result_msg_CalculateWeightManWithSDRA1 = NSLocalizedString("result_msg_CalculateWeightManWithSDRA1", comment: "")
    static let result_msg_CalculateFR = NSLocalizedString("result_msg_CalculateFR", comment: "")
    static let result_msg_CalculatePresionParcialO2B = NSLocalizedString("result_msg_CalculatePresionParcialO2B", comment: "")
    static let result_msg_CalculateDiferenciaAlveoloArterialO2 = NSLocalizedString("result_msg_CalculateDiferenciaAlveoloArterialO2", comment: "")
    static let result_msg_CalculateRelacionOxigenaciónO2 = NSLocalizedString("result_msg_CalculateRelacionOxigenaciónO2", comment: "")
    static let result_msg_CalculateCortocircuitosIntrapulmonares = NSLocalizedString("result_msg_CalculateCortocircuitosIntrapulmonares", comment: "")
    static let result_msg_PPEP_initial_1 = NSLocalizedString("result_msg_PPEP_initial_1", comment: "")
    static let result_msg_PEEP_initial_2 = NSLocalizedString("result_msg_PEEP_initial_2", comment: "")
    static let result_msg_PEEP_initial_3 = NSLocalizedString("result_msg_PEEP_initial_3", comment: "")
    
    static let result_msg_1 = NSLocalizedString("result_msg_1", comment: "")
    static let result_msg_2 = NSLocalizedString("result_msg_2", comment: "")
    static let result_msg_3 = NSLocalizedString("result_msg_3", comment: "")
    
    static let result_msg_pmv_1 = NSLocalizedString("result_msg_pmv_1", comment: "")
    static let result_msg_pmv_2 = NSLocalizedString("result_msg_pmv_2", comment: "")
    
    static let result_msg_dp_1 = NSLocalizedString("result_msg_dp_1", comment: "")
    static let result_msg_dp_2 = NSLocalizedString("result_msg_dp_2", comment: "")
    
    
    static let result_msg_safi_1 = NSLocalizedString("result_msg_safi_1", comment: "")
    static let result_msg_safi_2 = NSLocalizedString("result_msg_safi_2", comment: "")

    static let result_msg_indice_rox_1 = NSLocalizedString("result_msg_indice_rox_1", comment: "")
    static let result_msg_indice_rox_2 = NSLocalizedString("result_msg_indice_rox_2", comment: "")
    static let result_msg_indice_rox_3 = NSLocalizedString("result_msg_indice_rox_3", comment: "")

    static let result_msg_ratio_1 = NSLocalizedString("result_msg_ratio_1", comment: "")
    static let result_msg_ratio_2 = NSLocalizedString("result_msg_ratio_2", comment: "")
    static let result_msg_CalculateEspacioMuerto = NSLocalizedString("result_msg_CalculateEspacioMuerto", comment: "")
    static let result_msg_rox_1 = NSLocalizedString("result_msg_rox_1", comment: "")
    static let result_msg_hacor_1 = NSLocalizedString("result_msg_hacor_1", comment: "")
    static let result_msg_hacor_2 = NSLocalizedString("result_msg_hacor_2", comment: "")
    static let result_msg_wob_1 = NSLocalizedString("result_msg_wob_1", comment: "")
    static let result_msg_wob_2 = NSLocalizedString("result_msg_wob_2", comment: "")

}
