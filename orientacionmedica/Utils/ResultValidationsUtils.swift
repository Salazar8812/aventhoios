//
//  ResultValidationsUtils.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ResultValidationsUtils: NSObject {
    
    static func validateForPEEPInitial(result : Double) -> String {
        if (result < 30){
        return StringDescriptions.result_msg_PPEP_initial_1
        }else if (result > 29 && result < 41){
        return StringDescriptions.result_msg_PEEP_initial_2
        }else{
            return StringDescriptions.result_msg_PEEP_initial_3
        }
    }
    
    static func validateRRS(result : Double) -> String{
            if (result > 105){
        return StringDescriptions.result_msg_1
            }else if (result > 61 && result < 105){
        return StringDescriptions.result_msg_2
            }else if (result < 60){
        return StringDescriptions.result_msg_3
            }else {
            return ""
        }
    }
    
    static func validatePMV(result: Double)-> String {
            if (result < 12){
        return StringDescriptions.result_msg_pmv_1
            }else if (result > 12){
        return StringDescriptions.result_msg_pmv_2
            }else {
            return ""
        }
    }
    
    static func validateDP(result : Double) -> String{
            if (result < 13){
        return StringDescriptions.result_msg_dp_1
            }else if (result > 13){
        return StringDescriptions.result_msg_dp_2
            }else {
            return "";
        }
    }
    
    
    static func CalculateSaO2FiO2(result: Double) -> String {
        if result < 190{
            return StringDescriptions.result_msg_safi_1
        }else
        if result > 190{
            return StringDescriptions.result_msg_safi_2
        }else {
            return "Sin recomendación :(";
        }
    }

    static func CalculateIndiceROX(result: Double) -> String {
        if result > 4.88{
        return StringDescriptions.result_msg_indice_rox_1
        }else
            if result < 3.85{
            return StringDescriptions.result_msg_indice_rox_2
            }else if result > 3.85 && result < 3.88 {
            return StringDescriptions.result_msg_indice_rox_3
        }else {
            return "Sin recomendación :(";
        }
    }

    static func CalculateVentilatoryRatio(result: Double) -> String{
        /*if result >= 1.4 && result < 2{
            return StringDescriptions.result_msg_ratio_1
        } else
            if result >= 2{
            return StringDescriptions.result_msg_ratio_2
            }else {
            return "Sin recomendación :(";
        }*/
        
        return StringDescriptions.result_msg_ratio_1 + "\n" + StringDescriptions.result_msg_ratio_2
    }
    
    
    static func CalculateROX(result : Double) -> String {
        if (result < 6.6){
            return StringDescriptions.result_msg_rox_1
        }else {
               return "Sin recomendación :("
           }
       }

    static func CalculateHACOR(result : Double) -> String{
        if (result > 5){
            return StringDescriptions.result_msg_hacor_1
        }else if (result <= 5){
            return StringDescriptions.result_msg_hacor_2
        }else {
               return "Sin recomendación :("
           }
       }

    static func CalculateWobScore(result : Double) -> String{
        if (result > 4){
            return StringDescriptions.result_msg_wob_1
        }else if (result <= 4){
            return StringDescriptions.result_msg_wob_2
        } else {
               return "Sin recomendación :("
           }
       }

}
