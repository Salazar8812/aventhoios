//
//  ExSlideMenuController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 06/02/20.
//  Copyright © 2020 avento. All rights reserved.
//

import UIKit

class ExSlideMenuController: SlideMenuController {
    
    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is SecondHomeViewController ||
                vc is FormulasViewController ||
                vc is CommentsViewController ||
                vc is ContactViewController {
                return true
            }
        }
        return false
    }
    
}
